package uzoltan.readout.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import uzoltan.readout.R;

public class CustomFontHelper {

    /**
     * Sets a font on a textview based on the custom com.my.package:font attribute
     * If the custom font attribute isn't found in the attributes nothing happens
     * @param textview
     * @param context
     * @param attrs
     */
    public static void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        try{
            String font = a.getString(R.styleable.CustomFont_font);
            setCustomFont(textview, font, context);
        }
        finally {
            a.recycle();
        }

    }

    public static void setCustomFont(TextView textview, String font, Context context) {
        if(font == null) {
            return;
        }
        Typeface tf = FontCache.get(font, context);
        //Typeface tf = Typeface.createFromAsset(context.getAssets(), font);
        if(tf != null) {
            textview.setTypeface(tf);
        }
    }

}
