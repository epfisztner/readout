package uzoltan.readout.misc;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;

public class ReadOutUtilities {

    public static String formatPhoneNumber(String phoneNumber) {

        String formattedNumber;

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            formattedNumber = PhoneNumberUtils.formatNumber(phoneNumber, "HU");
//        } else {
            if (phoneNumber.length() == 12 && phoneNumber.startsWith("+")) {
                formattedNumber = phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3, 5) + " " + phoneNumber.substring(5, 8) + " " + phoneNumber.substring(8, 12);
            } else if (phoneNumber.length() == 11 && phoneNumber.startsWith("06")) {
                formattedNumber = "+36 " + phoneNumber.substring(2, 4) + " " + phoneNumber.substring(4, 7) + " " + phoneNumber.substring(7, 11);
            } else {
                formattedNumber = phoneNumber;
            }
//        }
        return formattedNumber;
    }

//    private String formatPhoneNumber(String phoneNumber) {
//
//        String formattedNumber;
//
//        if (phoneNumber.length() == 12 && phoneNumber.startsWith("+")) {
//            formattedNumber = phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3, 5) + " " + phoneNumber.substring(5, 8) + " " + phoneNumber.substring(8, 12);
//        } else if (phoneNumber.length() == 11 && phoneNumber.startsWith("06")) {
//            formattedNumber = "+36 " + phoneNumber.substring(2, 4) + " " + phoneNumber.substring(4, 7) + " " + phoneNumber.substring(7, 11);
//        } else {
//            formattedNumber = phoneNumber;
//        }
//        return formattedNumber;
//    }


    public static String getContactName(Context context, String phoneNumber) {

        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        int indexName = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        try {
            if (cur.moveToFirst()) {
                return cur.getString(indexName);
            }
        } finally {
            cur.close();
        }
        return formatPhoneNumber(phoneNumber);
    }

    public static boolean isOnline(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();
        } else {
            return false;
        }
    }
}
