package uzoltan.readout.asynctask;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import uzoltan.readout.R;

public class SendEmail extends AsyncTask<String, Integer, String> {

    private ProgressDialog progress;
    private String to, from, subject, content, username, password, smtpHostName, smtpPort;
    private Context ctx;

    public SendEmail(String[] parameters, Context ctx){
        this.to = parameters[0];
        this.from = parameters[1];
        this.subject = parameters[2];
        this.content = parameters[3];
        this.username = parameters[1];
        this.password = parameters[4];
        this.smtpHostName = parameters[5];
        this.smtpPort = parameters[6];
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(ctx, null, ctx.getString(R.string.email_sending_progress_dialog), true);
        progress.setCancelable(true);
        progress.setOnCancelListener(new ProgressDialog.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
                Toast.makeText(ctx, R.string.email_sending_canceled_toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected String doInBackground(String... params) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", smtpHostName);
        props.put("mail.smtp.port", smtpPort);

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object.
            javax.mail.Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.setRecipients(javax.mail.Message.RecipientType.TO,
                    InternetAddress.parse(to));

            message.setSubject(subject);

            message.setText(content);

            // Send message
            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
            return "exception";
        }

        return "sent";
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progress.dismiss();

        if(result.equals("sent")){
            Toast.makeText(ctx, R.string.email_successfully_sent_toast, Toast.LENGTH_LONG).show();
        }
        else if(result.equals("exception")){
            Toast.makeText(ctx, R.string.email_sending_failed_toast, Toast.LENGTH_LONG).show();
        }
    }
}
