package uzoltan.readout.asynctask;

import android.os.AsyncTask;

import uzoltan.readout.misc.TextToSpeech;

public class ReadLongText extends AsyncTask<String, Integer, Void> {

    private TextToSpeech tts = null;


    public ReadLongText(TextToSpeech tts) {
        this.tts = tts;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public static String[] splitStringIntoSentences(String text) {
        String[] Sentences;
        Sentences = text.split("\\. ");
        return Sentences;
    }

    @Override
    protected Void doInBackground(String... params) {

        String toSay = params [0];

        do {
            String[] Sentences = splitStringIntoSentences(toSay);
            String toSpeak = "";
            int breakingPoint = Sentences.length;
            for (int i = 0; toSpeak.length() + Sentences[i].length() < 4000; i++) {
                toSpeak = toSpeak.concat(Sentences[i] + ". ");
                if (i == breakingPoint - 1)
                    break;
            }
            tts.speakText_add(toSpeak, "requestID");
            StringBuilder leftOver = new StringBuilder();
            for (int j = 0; j < Sentences.length; j++) {
                leftOver.append(Sentences[j]);
                leftOver.append(". ");
            }
            toSay = leftOver.toString().substring(toSpeak.length());

        } while (!toSay.isEmpty());


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
