package uzoltan.readout.asynctask;

import java.util.List;

import uzoltan.readout.sms.SmsConversation;

public interface FetchConversationsInterface {

    void AsyncTaskFinish(List<SmsConversation> smsConversations);
}
