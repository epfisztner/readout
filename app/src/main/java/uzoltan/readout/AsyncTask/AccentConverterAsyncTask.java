package uzoltan.readout.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import uzoltan.readout.misc.AccentConverter;

public class AccentConverterAsyncTask extends AsyncTask<Void, Void, AccentConverter> {


    private Context context;

    public AccentConverterAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected AccentConverter doInBackground(Void... params) {
        return new AccentConverter(context);
    }
}
