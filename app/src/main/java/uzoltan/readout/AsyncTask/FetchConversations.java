package uzoltan.readout.asynctask;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import uzoltan.readout.misc.AccentConverter;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.sms.SmsConversation;
import uzoltan.readout.sms.SmsMessage;

public class FetchConversations extends AsyncTask<Void, Integer, List<SmsConversation>> {

    List<SmsConversation> smsConversations;
    public FetchConversationsInterface delegate = null;
    private Context context;

    public FetchConversations(Context context) {
        this.context = context;
        this.delegate = (FetchConversationsInterface) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<SmsConversation> doInBackground(Void... params) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        smsConversations = new ArrayList<>();
        Uri uriSms = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            uriSms = Telephony.Sms.Conversations.CONTENT_URI;
        } else {
            uriSms = Uri.parse("content://sms/conversations");
        }

        String[] projection = new String[]{
//                Telephony.TextBasedSmsColumns.ADDRESS,
                Telephony.TextBasedSmsColumns.THREAD_ID,
//                Telephony.TextBasedSmsColumns.PERSON,
//                Telephony.TextBasedSmsColumns.BODY,
//                Telephony.TextBasedSmsColumns.DATE,
//                Telephony.TextBasedSmsColumns.TYPE,
//                Telephony.TextBasedSmsColumns.READ,
//                Telephony.TextBasedSmsColumns.SEEN
                Telephony.Sms.Conversations.SNIPPET,
                Telephony.Sms.Conversations.MESSAGE_COUNT
        };
        Cursor cursor = context.getContentResolver()
                .query(uriSms, projection, null, null, null);
        if (cursor != null) {

            if (cursor != null) {

                if (cursor.moveToFirst()) {
                    do {
                        SmsConversation smsConversation = new SmsConversation();

                        smsConversation.setThreadId(cursor.getInt(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.THREAD_ID)));
                        smsConversation.setMessageCount(cursor.getInt(cursor.getColumnIndex(Telephony.Sms.Conversations.MESSAGE_COUNT)));
                        smsConversation.setSnippet(cursor.getString(cursor.getColumnIndex(Telephony.Sms.Conversations.SNIPPET)));
//                        smsConversation.setPartnerPhoneNumber(cursor.getString(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.ADDRESS)));

                        smsConversations.add(smsConversation);
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            }
        }
        return smsConversations;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(List<SmsConversation> smsConversations) {
        super.onPostExecute(smsConversations);
        delegate.AsyncTaskFinish(smsConversations);
    }

    public int getContactIDFromNumber(String contactNumber, Context context) {
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            String id = contactLookupCursor.getString(
                    contactLookupCursor.getColumnIndex(ContactsContract.Contacts._ID));
            String phoneNo = null;
            if (Integer.parseInt(contactLookupCursor.getString(contactLookupCursor.getColumnIndex(
                    ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                Cursor pCur = context.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                        new String[]{id}, null);
                while (pCur.moveToNext()) {
                    phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
                pCur.close();
                if (phoneNo.equals(contactNumber)) {
                    phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
            }
        }
        contactLookupCursor.close();

        return phoneContactID;
    }

}
