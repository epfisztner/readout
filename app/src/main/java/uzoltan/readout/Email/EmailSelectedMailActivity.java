package uzoltan.readout.email;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uzoltan.readout.asynctask.FetchEmailContent;
import uzoltan.readout.asynctask.FetchEmailContentInterface;
import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.R;

public class EmailSelectedMailActivity extends AbstractReadOutActivity implements FetchEmailContentInterface, SensorEventListener {

    public static final String EMAIL_FOLDER = "email_type";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String EMAIL_SUBJECT = "email_subject";
    public static final String EMAIL_DATE = "email_date";
    public static final String EMAIL_CONTENT = "email_content";
    public static final String EMAIL_UID = "email_uid";

    private TextView mAddress;
    private TextView mSubject;
    private TextView mDate;
    private WebView mContent;
    private String email;
    private String password;
    private String imapHostName;
    private String emailMessageBody;
    private boolean isFaceDown;
    private int messageType;
    private long uid;
    private FetchEmailContent mAsyncTask;
    private com.getbase.floatingactionbutton.FloatingActionButton mReplyFAB;
    private com.getbase.floatingactionbutton.FloatingActionButton mTTSSpeaker;
    private SensorManager mSensorManager;
    private Sensor mGravitySensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email__selected_mail);
        initActivityTitle(getString(R.string.title_activity_email__selected_mail));
        initToolBar();

        //init views
        mAddress = (TextView) findViewById(R.id.email_address);
        mSubject = (TextView) findViewById(R.id.email_subject);
        mDate = (TextView) findViewById(R.id.email_date);
        mContent = (WebView) findViewById(R.id.email_content);
        mReplyFAB = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.reply_fab);
        mTTSSpeaker = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.tts_speaker_fab);

        mContent.getSettings().setJavaScriptEnabled(true);
        mContent.addJavascriptInterface(new MyJavaScriptInterface(), "INTERFACE");
        mContent.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:window.INTERFACE.processContent(document.getElementsByTagName('body')[0].innerText);");
            }
        });

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        //getting intent info
        Intent intent = getIntent();
        Bundle emailData = intent.getExtras();
        messageType = emailData.getInt(EMAIL_FOLDER);
        mAddress.setText(emailData.getString(EMAIL_ADDRESS));
        mSubject.setText(emailData.getString(EMAIL_SUBJECT));
        mDate.setText(emailData.getString(EMAIL_DATE));
        mContent.loadData(emailData.getString(EMAIL_CONTENT), "text/html; charset=UTF-8", null);

        //getting the email account info
        SharedPreferences sharedPref = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        email = sharedPref.getString("email", "");
        password = sharedPref.getString("password", "");
        imapHostName = sharedPref.getString("imap_hostname", "");

        //this is a sent email
//        if (messageType == 2) {
//            mContent.loadData(emailData.getString(EMAIL_CONTENT), "text/html; charset=UTF-8", null);
//        }

//        this is a received email
//        else if (messageType == 1) {
//            uid = emailData.getLong(EMAIL_UID);
//            String UID = String.valueOf(uid);
//            List<EmailMessage> emailMessage = EmailMessage.find(EmailMessage.class, "u_id = ?", UID);
//            if (emailMessage.size() > 0) {
//                if (emailMessage.get(0).getMessageContent() != null && !emailMessage.get(0).getMessageContent().equals("")) {
//                    mContent.loadData(emailMessage.get(0).getMessageContent(), "text/html; charset=UTF-8", null);
//                }
//            } else {
//                if (isOnline()) {
//                    mAsyncTask = new FetchEmailContent(EmailSelectedMailActivity.this, email, password, imapHostName, uid);
//                    mAsyncTask.delegate = EmailSelectedMailActivity.this;
//                    mAsyncTask.execute();
//                } else {
//                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
//                    readText(getString(R.string.no_internet_connection));
//                }
//            }
//        }

        mReplyFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailSelectedMailActivity.this, EmailNewActivity.class);
                intent.putExtra(EMAIL_ADDRESS, mAddress.getText().toString());
                intent.putExtra(EMAIL_SUBJECT, mSubject.getText().toString());
                finish();
                startActivity(intent);
            }
        });
    }

    @Override
    protected void initToolBar() {
        //setting up the toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGravitySensor != null) {
            mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mContent.canGoBack()) {
                        mContent.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.email_selected, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (android.R.id.home == id) {
            onBackPressed();
        }

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            } catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(EmailSelectedMailActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void AsyncTaskFinish(String emailContent) {
        mContent.loadData(emailContent, "text/html; charset=UTF-8", null);
        String UID = String.valueOf(uid);
        List<EmailMessage> emailMessage = EmailMessage.find(EmailMessage.class, "u_id = ?", UID);
        if (emailMessage.get(0) != null) {
            emailMessage.get(0).setMessageContent(emailContent);
            emailMessage.get(0).update();
        }
    }

    protected String applyHTMLtags(String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        return strBuilder.toString();
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isNavigationAssistOn = sharedPref.getBoolean("pref_navigation_assist", false);

        mTTSSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toSay = getString(R.string.tts_from) + mAddress.getText().toString() + ", " + getString(R.string.tts_subject)
                        + mSubject.getText().toString() + ", " + getString(R.string.tts_content) + emailMessageBody;

                readText(toSay);
            }
        });

        mTTSSpeaker.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                tts.stopSpeak();
                return true;
            }
        });

        if (isNavigationAssistOn) {

            mAddress.setOnLongClickListener(new TextView.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    readText(mAddress.getText().toString());
                    return true;
                }
            });

            mSubject.setOnLongClickListener(new TextView.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    String toSay = mSubject.getText().toString();
                    if (toSay.isEmpty()) {
                        readText(getString(R.string.tts_empty_subject_field));
                    } else {
                        readText(mSubject.getText().toString());
                    }
                    return true;
                }
            });

            mDate.setOnLongClickListener(new TextView.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    readText(mDate.getText().toString());
                    return true;
                }
            });
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        final float factor = 0.85F; //recommended value is between 0.7-0.95.
        if (event.sensor == mGravitySensor) {
            boolean nowDown = event.values[2] < -SensorManager.GRAVITY_EARTH * factor;
            if (nowDown != isFaceDown) {
                isFaceDown = nowDown;  //update our global variable
            }

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean enableTurnOver = sharedPref.getBoolean("pref_turn_around_to_silence", false);
            if (enableTurnOver) {
                if (isFaceDown)
                    tts.stopSpeak();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //needed to implement SensorEventListener
    }

    class MyJavaScriptInterface {

        public MyJavaScriptInterface() {
        }

        @JavascriptInterface
        public void processContent(String aContent) {
            emailMessageBody = aContent;
        }
    }
}
