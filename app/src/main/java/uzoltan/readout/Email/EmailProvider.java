package uzoltan.readout.email;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmailProvider extends SugarRecord {

    private String providerName;
    private String imapHostName;
    private String smtpHostName;
    private int smtpPort;

}