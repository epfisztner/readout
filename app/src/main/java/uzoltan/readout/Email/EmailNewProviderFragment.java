package uzoltan.readout.email;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import uzoltan.readout.misc.ReadOutApplication;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.misc.TextToSpeech;
import uzoltan.readout.R;

public class EmailNewProviderFragment extends DialogFragment {

    // Log tag
    public static final String TAG = "NewProviderFragment";

    // UI
    private TextView DomainNameTV;
    private TextView IMAPHostnameTV;
    private TextView SMTPHostnameTV;
    private TextView SMTPPortTV;
    private EditText mDomainName;
    private EditText mIMAPHostName;
    private EditText mSMTPHostName;
    private EditText mSMTPPort;
    private Button mCancelButton;
    private Button mAddButton;

    private Context context;

    // Listener interface
    private INewProviderFragment listener;

    private TextToSpeech tts =null;

    public EmailNewProviderFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ReadOutApplication application = (ReadOutApplication) getActivity().getApplication();
        tts = new TextToSpeech(application);
        context = (Context) activity;

        if (getTargetFragment() != null) {
            try {
                listener = (INewProviderFragment) getTargetFragment();
            } catch (ClassCastException ce) {
                Log.e(TAG,
                        "Target Fragment does not implement fragment interface!");
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception!");
                e.printStackTrace();
            }
        } else {
            try {
                listener = (INewProviderFragment) activity;
            } catch (ClassCastException ce) {
                Log.e(TAG,
                        "Parent Activity does not implement fragment interface!");
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception!");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        tts.stopSpeak();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        setLocale();

        View root = inflater.inflate(R.layout.add_new_provider_fragment, container, false);

        // Setting dialog title
        getDialog().setTitle(R.string.new_provider_dialog_title);

        //init views
        DomainNameTV = (TextView) root.findViewById(R.id.domain_name_tv);
        IMAPHostnameTV = (TextView) root.findViewById(R.id.imap_hostname_tv);
        SMTPHostnameTV = (TextView) root.findViewById(R.id.smtp_hostname_tv);
        SMTPPortTV = (TextView) root.findViewById(R.id.smtp_port_tv);
        mDomainName = (EditText) root.findViewById(R.id.domain_name);
        mIMAPHostName = (EditText) root.findViewById(R.id.imap_hostname);
        mSMTPHostName = (EditText) root.findViewById(R.id.smtp_hostname);
        mSMTPPort = (EditText) root.findViewById(R.id.smtp_port);
        mCancelButton = (Button) root.findViewById(R.id.btnCancel);
        mAddButton = (Button) root.findViewById(R.id.btnAddProvider);

        handleTTScalls();

        mCancelButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mAddButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(mDomainName.getText().toString().isEmpty() || mIMAPHostName.getText().toString().isEmpty() ||
                        mSMTPHostName.getText().toString().isEmpty() || mSMTPPort.getText().toString().isEmpty()){
                    Toast.makeText(context, R.string.new_provider_input_error_toast, Toast.LENGTH_LONG).show();
                    readText(getString(R.string.new_provider_input_error_toast));
                }
                else{
                    EmailProvider emailProvider = new EmailProvider();
                    emailProvider.setProviderName(mDomainName.getText().toString());
                    emailProvider.setImapHostName(mIMAPHostName.getText().toString());
                    emailProvider.setSmtpHostName(mSMTPHostName.getText().toString());
                    emailProvider.setSmtpPort(Integer.parseInt(mSMTPPort.getText().toString()));

                    if (listener != null) {
                        listener.addProviderToDatabase(emailProvider);
                    }

                    dismiss();
                }
            }
        });


        return root;
    }

    public void setLocale(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String mLanguagePrefID = sharedPref.getString(SharedPreferencesUtils.KEY_LANGUAGE, "1");
        Locale locale;
        switch (mLanguagePrefID) {
            case "1":
                locale = new Locale("en_US");
                break;
            case "2":
                locale = new Locale("hu");
                break;
            default:
                locale= new Locale("en_US");
        }
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }

    // Listener interface
    public interface INewProviderFragment {
        public void addProviderToDatabase(EmailProvider emailProvider);
    }

    public void handleTTScalls(){

        DomainNameTV.setOnLongClickListener(new TextView.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                readText(DomainNameTV.getText().toString());
                return true;
            }
        });

        IMAPHostnameTV.setOnLongClickListener(new TextView.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(IMAPHostnameTV.getText().toString());
                return true;
            }
        });

        SMTPHostnameTV.setOnLongClickListener(new TextView.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                readText(SMTPHostnameTV.getText().toString());
                return true;
            }
        });

        SMTPPortTV.setOnLongClickListener(new TextView.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                readText(SMTPPortTV.getText().toString());
                return true;
            }
        });

        mDomainName.setOnLongClickListener(new EditText.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                String toSay = mDomainName.getText().toString();
                if(toSay.isEmpty()){
                    readText(context.getString(R.string.tts_empty_edittext));
                }
                else{
                    readText(toSay);
                }
                return true;
            }
        });

        mIMAPHostName.setOnLongClickListener(new EditText.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                String toSay = mIMAPHostName.getText().toString();
                if(toSay.isEmpty()){
                    readText(context.getString(R.string.tts_empty_edittext));
                }
                else{
                    readText(toSay);
                }
                return true;
            }
        });

        mSMTPHostName.setOnLongClickListener(new EditText.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                String toSay = mSMTPHostName.getText().toString();
                if(toSay.isEmpty()){
                    readText(context.getString(R.string.tts_empty_edittext));
                }
                else{
                    readText(toSay);
                }
                return true;
            }
        });

        mSMTPPort.setOnLongClickListener(new EditText.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                String toSay = mSMTPPort.getText().toString();
                if(toSay.isEmpty()){
                    readText(context.getString(R.string.tts_empty_edittext));
                }
                else{
                    readText(toSay);
                }
                return true;
            }
        });

        mAddButton.setOnLongClickListener(new Button.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                readText(context.getString(R.string.tts_add_button));
                return true;
            }
        });

        mCancelButton.setOnLongClickListener(new Button.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                readText(context.getString(R.string.tts_cancel_button));
                return true;
            }
        });
    }

    public void readText(String toSay){

        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        Boolean isHeadsetOn = am.isWiredHeadsetOn();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean isNavigationAssistOn = sharedPref.getBoolean("pref_navigation_assist", false);
        Boolean ignoreSilentMode = sharedPref.getBoolean("pref_ignore_silent_email", false);

        if(isNavigationAssistOn){
            if(isHeadsetOn){
                tts.speakText_flush(toSay, "requestID");
            }
            else{
                if(ignoreSilentMode){
                    tts.speakText_flush(toSay, "requestID");
                }
                else{
                    switch (am.getRingerMode()) {
                        case AudioManager.RINGER_MODE_SILENT:
                            break;
                        case AudioManager.RINGER_MODE_VIBRATE:
                            break;
                        case AudioManager.RINGER_MODE_NORMAL:
                            tts.speakText_flush(toSay, "requestID");
                            break;
                    }
                }
            }
        }
    }
}

