package uzoltan.readout.email;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmailMessage extends SugarRecord {

    private String messageFolder;
    private long uID;
    private boolean isRead;
    private String messageAddress;
    private String messageDate;
    private String messageSubject;
    private String messageContent;

}
