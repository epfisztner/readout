package uzoltan.readout.email;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uzoltan.readout.asynctask.FetchEmailFolders;
import uzoltan.readout.asynctask.FetchEmailFoldersInterface;
import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.R;

public class EmailLoginActivity extends AbstractReadOutActivity implements FetchEmailFoldersInterface, EmailNewProviderFragment.INewProviderFragment {

    private EditText mEmailInput, mPasswordInput;
    private CheckBox mCheckbox;
    private Button mLoginButton;
    private SharedPreferences sharedPreferences = null;
    private String userName;
    private String password;
    private String imapHost;
    private String providerName;
    private String smtpHost;
    private int smtpPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.activity_email__login);
        initActivityTitle(getString(R.string.title_activity_email_login));
        initToolBar();

        mEmailInput = (EditText) findViewById(R.id.email_input);
        mPasswordInput = (EditText) findViewById(R.id.password_input);
        mCheckbox = (CheckBox) findViewById(R.id.save_checkbox);
        mLoginButton = (Button) findViewById(R.id.login_button);

        //saving the login info
        mCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCheckbox.isChecked()) {
                    sharedPreferences.edit()
                            .putString(SharedPreferencesUtils.KEY_SAVED_EMAIL, mEmailInput.getText().toString())
                            .putString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, mPasswordInput.getText().toString())
                            .apply();
                    Snackbar.make(view, R.string.snackbar_saved_text, Snackbar.LENGTH_SHORT).show();
                    readText(getString(R.string.snackbar_saved_text));
                }
            }
        });

        //restoring the saved login info
        userName = sharedPreferences.getString(SharedPreferencesUtils.KEY_SAVED_EMAIL, "");
        password = sharedPreferences.getString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, "");

        if (!userName.isEmpty() && !password.isEmpty()) {
            mEmailInput.setText(userName);
            int position = userName.length();
            mEmailInput.setSelection(position);
            mPasswordInput.setText(password);
        }

        //if we launch the app for the first time, gmail as an email provider added to the database
        if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_APP_FIRST_TIME_LAUNCHED, true)) {
            EmailProvider emailProvider = new EmailProvider();
            emailProvider.setProviderName("gmail.com");
            emailProvider.setImapHostName("imap.gmail.com");
            emailProvider.setSmtpHostName("smtp.gmail.com");
            emailProvider.setSmtpPort(587);
            emailProvider.save();
            sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_APP_FIRST_TIME_LAUNCHED, false).apply();
        }

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmailInput.getText().toString().isEmpty())
                    mEmailInput.setError(getString(R.string.email_input_error));
                else if (mPasswordInput.getText().toString().isEmpty())
                    mPasswordInput.setError(getString(R.string.password_input_error));
                else {
                    connect(mEmailInput.getText().toString(), mPasswordInput.getText().toString(), false);
                }
            }
        });

        if (!userName.equals("") && !password.equals("")) {
            connect(userName, password, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.email__login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_new_provider) {
            readText(getString(R.string.action_new_provider));

            EmailNewProviderFragment newProviderFragment = new EmailNewProviderFragment();
            FragmentManager fm = getSupportFragmentManager();
            newProviderFragment.show(fm, EmailNewProviderFragment.TAG);
        }

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            } catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(EmailLoginActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void connect(String userName, String password, boolean savedUser) {
        int atIndex = userName.lastIndexOf("@");
        providerName = userName.substring(atIndex + 1);
        List<EmailProvider> emailProviders = EmailProvider.listAll(EmailProvider.class);

        EmailProvider emailProvider = new EmailProvider();
        for (EmailProvider tempEmailProvider : emailProviders) {
            if (providerName.equals(tempEmailProvider.getProviderName())) {
                emailProvider = tempEmailProvider;
                break;
            }
        }
        if (!savedUser) {
            sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_FIRST_EMAIL_LOAD, true).apply();

            if (isOnline()) {
                if (emailProvider.getProviderName() == null) {
                    Toast.makeText(this, R.string.unknown_provider, Toast.LENGTH_LONG).show();
                    readText(getString(R.string.unknown_provider));
                } else {
                    imapHost = emailProvider.getImapHostName();
                    smtpHost = emailProvider.getSmtpHostName();
                    smtpPort = emailProvider.getSmtpPort();
                    this.userName = userName;
                    this.password = password;
                    FetchEmailFolders fetchMailFolders = new FetchEmailFolders(this, userName, password, imapHost);
                    fetchMailFolders.setDelegate(this);
                    fetchMailFolders.execute((Void[]) null);
                }
            } else {
                if ((int) EmailMessage.count(EmailMessage.class, null, null) == 0) {
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                    readText(getString(R.string.no_internet_connection));
                } else {
                    mHeaderText.setText(mEmailInput.getText().toString());
                    Intent intent = new Intent(EmailLoginActivity.this, EmailFolderActivity.class);
                    startActivity(intent);
                }
            }

        } else {
            sharedPreferences.edit()
                    .putString(SharedPreferencesUtils.KEY_SAVED_EMAIL, userName)
                    .putString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, password)
                    .putString(SharedPreferencesUtils.KEY_PROVIDER_NAME, emailProvider.getProviderName())
                    .putString(SharedPreferencesUtils.KEY_IMAP_HOSTNAME, emailProvider.getImapHostName())
                    .putString(SharedPreferencesUtils.KEY_SMTP_HOSTNAME, emailProvider.getSmtpHostName())
                    .putInt(SharedPreferencesUtils.KEY_SMTP_PORT, emailProvider.getSmtpPort())
                    .putBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, true)
                    .apply();
            mHeaderText.setText(mEmailInput.getText().toString());
            Intent intent = new Intent(this, EmailFolderActivity.class);
            intent.putExtra(EmailFolderActivity.EMAIL_FOLDER, "");
            startActivity(intent);
        }
    }

    @Override
    public void addProviderToDatabase(EmailProvider emailProvider) {
        emailProvider.update();
        Toast.makeText(EmailLoginActivity.this, R.string.provider_added_toast, Toast.LENGTH_SHORT).show();
        readText(getString(R.string.provider_added_toast));
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        mEmailInput.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mEmailInput.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mPasswordInput.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mPasswordInput.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mCheckbox.setOnLongClickListener(new CheckBox.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(getString(R.string.tts_save_checkbox));
                return true;
            }
        });

        mLoginButton.setOnLongClickListener(new Button.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(getString(R.string.tts_login_button));
                return true;
            }
        });
    }

    @Override
    public void AsyncTaskFinish(Boolean loginSucced) {
        if (loginSucced) {
            sharedPreferences.edit()
                    .putString(SharedPreferencesUtils.KEY_SAVED_EMAIL, userName)
                    .putString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, password)
                    .putString(SharedPreferencesUtils.KEY_PROVIDER_NAME, providerName)
                    .putString(SharedPreferencesUtils.KEY_IMAP_HOSTNAME, imapHost)
                    .putString(SharedPreferencesUtils.KEY_SMTP_HOSTNAME, smtpHost)
                    .putInt(SharedPreferencesUtils.KEY_SMTP_PORT, smtpPort)
                    .putBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, true)
                    .apply();

            mHeaderText.setText(mEmailInput.getText().toString());
            Intent intent = new Intent(EmailLoginActivity.this, EmailFolderActivity.class);
            intent.putExtra(EmailFolderActivity.EMAIL_FOLDER, "");
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.login_failed_toast), Toast.LENGTH_LONG).show();
            readText(getString(R.string.login_failed_toast));
        }
    }
}