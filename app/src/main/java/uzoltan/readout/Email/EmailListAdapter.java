package uzoltan.readout.email;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.misc.TextToSpeech;
import uzoltan.readout.R;

public class EmailListAdapter extends RecyclerView.Adapter<EmailListAdapter.EmailViewHolder> {

    private LinkedList<EmailMessage> emailList;
    private ViewGroup viewGroup;
    private TextToSpeech tts;

    public EmailListAdapter(LinkedList<EmailMessage> emailList, TextToSpeech tts) {
        this.emailList = emailList;
        this.tts = tts;
    }

    @Override
    public EmailListAdapter.EmailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.email_list_row, parent, false);
        viewGroup = parent;
        return new EmailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EmailListAdapter.EmailViewHolder holder, int position) {
        final EmailMessage email = emailList.get(position);
        holder.vEmailAddress.setText(email.getMessageAddress());
        holder.vEmailSubject.setText(email.getMessageSubject());
        holder.vTTSSpeaker.setImageResource(R.drawable.ic_volume_up_24dp);
        holder.vEmailDate.setText(email.getMessageDate());
        holder.isRead = email.isRead();

        setEmailColor(holder, email);

        holder.vTTSSpeaker.setOnClickListener(new ImageButton.OnClickListener() {

            @Override
            public void onClick(View v) {
                Context context = viewGroup.getContext();

                AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                boolean isHeadsetOn = am.isWiredHeadsetOn();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                boolean ignoreSilentMode = sharedPref.getBoolean(SharedPreferencesUtils.KEY_IGNORE_SILENT_EMAIL, false);
                String toSay;

                toSay = email.getMessageFolder() + " " + email.getMessageAddress() + ", " +
                        viewGroup.getContext().getString(R.string.tts_subject) + email.getMessageSubject();

                if (isHeadsetOn) {
                    tts.speakText_flush(toSay, "requestID");
                    Toast.makeText(context, viewGroup.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
                } else {
                    if (ignoreSilentMode) {
                        tts.speakText_flush(toSay, "requestID");
                        Toast.makeText(context, viewGroup.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
                    } else {
                        switch (am.getRingerMode()) {
                            case AudioManager.RINGER_MODE_SILENT:
                                Toast.makeText(context, viewGroup.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                                break;
                            case AudioManager.RINGER_MODE_VIBRATE:
                                Toast.makeText(context, viewGroup.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                                break;
                            case AudioManager.RINGER_MODE_NORMAL:
                                tts.speakText_flush(toSay, "requestID");
                                Toast.makeText(context, viewGroup.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                }
            }
        });

        holder.vTTSSpeaker.setOnLongClickListener(new ImageButton.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                if (tts != null) {
                    tts.stopSpeak();
                }
                return true;
            }
        });

        holder.vCardView.setOnClickListener(new CardView.OnClickListener() {

            @Override
            public void onClick(View v) {
                Context context = viewGroup.getContext();
                Intent intent = new Intent(context, EmailSelectedMailActivity.class);
                Bundle emailData = new Bundle();
                emailData.putString(EmailSelectedMailActivity.EMAIL_FOLDER, email.getMessageFolder());
                emailData.putString(EmailSelectedMailActivity.EMAIL_ADDRESS, email.getMessageAddress());
                emailData.putString(EmailSelectedMailActivity.EMAIL_SUBJECT, email.getMessageSubject());
                emailData.putString(EmailSelectedMailActivity.EMAIL_DATE, email.getMessageDate());

                emailData.putString(EmailSelectedMailActivity.EMAIL_CONTENT, email.getMessageContent());
                intent.putExtras(emailData);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return emailList.size();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) viewGroup.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void setEmailColor(EmailViewHolder holder, EmailMessage email) {
        if (!email.isRead()) {
            holder.vCardView.setCardBackgroundColor(ContextCompat.getColor(viewGroup.getContext(), R.color.ThirdBackgroundColor));
            holder.vEmailAddress.setTypeface(null, Typeface.BOLD);
            holder.vEmailSubject.setTypeface(null, Typeface.BOLD);
        } else {
            holder.vCardView.setCardBackgroundColor(ContextCompat.getColor(viewGroup.getContext(), R.color.PrimaryBackgroundColor));
            holder.vEmailAddress.setTypeface(null, Typeface.NORMAL);
            holder.vEmailSubject.setTypeface(null, Typeface.NORMAL);
        }
    }

    public static class EmailViewHolder extends RecyclerView.ViewHolder {

        protected CardView vCardView;
        protected TextView vEmailAddress;
        protected TextView vEmailSubject;
        protected ImageButton vTTSSpeaker;
        protected TextView vEmailDate;
        protected boolean isRead;

        public EmailViewHolder(View v) {
            super(v);
            vCardView = (CardView) v.findViewById(R.id.email_card_view);
            vEmailAddress = (TextView) v.findViewById(R.id.email_address);
            vEmailSubject = (TextView) v.findViewById(R.id.email_subject);
            vTTSSpeaker = (ImageButton) v.findViewById(R.id.tts_speaker);
            vEmailDate = (TextView) v.findViewById(R.id.email_date);
        }
    }
}
