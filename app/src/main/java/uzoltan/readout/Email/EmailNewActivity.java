package uzoltan.readout.email;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import uzoltan.readout.asynctask.SendEmail;
import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.R;

public class EmailNewActivity extends AbstractReadOutActivity {

    private EditText mRecipients, mSubject, mMessage;
    private Button mSendButton;
    private SendEmail mAsynctask;
    private String email, password, smtpHostName;
    private Integer smtpPort;

    public static final int PICK_CONTACT_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email__new);
        initActivityTitle(getString(R.string.title_activity_email__new));
        initToolBar();
        // init views
        mRecipients = (EditText) findViewById(R.id.email_recipients_input);
        mSubject = (EditText) findViewById(R.id.subject_input);
        mMessage = (EditText) findViewById(R.id.message_input);
        mSendButton = (Button) findViewById(R.id.send_button);

        // getting email account data
        SharedPreferences sharedPref = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        email = sharedPref.getString("email", "");
        password = sharedPref.getString("password", "");
        smtpHostName = sharedPref.getString("smtp_hostname", "");
        smtpPort = sharedPref.getInt("smtp_port", 0);

        mHeaderText.setText(email);

        //getting intent info if possible
        Intent intent = getIntent();
        Bundle emailData = intent.getExtras();
        if (emailData != null) {
            mRecipients.setText(emailData.getString("message_address"));
            mSubject.setText("Re: " + emailData.getString("message_subject"));
            mMessage.requestFocus();
        }

        mSendButton.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                String recipients = mRecipients.getText().toString();
                if (recipients.equals("")) {
                    mRecipients.setError(getString(R.string.recipients_input_error));
                } else if (!recipients.contains("@")) {
                    mRecipients.setError(getString(R.string.recipients_input_error_2));
                } else {
                    final String[] newEmailParameters = {recipients, email, mSubject.getText().toString(), mMessage.getText().toString(), password, smtpHostName, smtpPort.toString()};
                    mAsynctask = new SendEmail(newEmailParameters, EmailNewActivity.this);
                    mAsynctask.execute();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {

            // Get the URI that points to the selected contact
            final Uri contactUri = data.getData();
            // We only need the ADDRESS column
            final String[] projection = {ContactsContract.CommonDataKinds.Email.ADDRESS};

            // Perform the query on the contact to get the ADDRESS column
            // We don't need a selection or sort order (there's only one result for the given URI)
            // The query() method is called from a separate thread to avoid blocking the app's UI thread.

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Cursor cursor = getContentResolver()
                            .query(contactUri, projection, null, null, null);

                    cursor.moveToFirst();

                    // Retrieve the email address from the address column
                    int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
                    final String email = cursor.getString(column);
                    cursor.close();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mRecipients.getText().toString().isEmpty()) {
                                mRecipients.setText(email);
                            } else {
                                mRecipients.append(", " + email);
                            }
                            int position = mRecipients.getText().length();
                            mRecipients.setSelection(position);
                        }
                    });
                }
            }).run();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.email__new, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_email_contacts) {
            tts.speakText_flush(getString(R.string.action_email_contacts), "requestID");

            Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));

            // Show user only contacts with email addresses
            pickContactIntent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
            startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
        }

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            }
            catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(EmailNewActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        mRecipients.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mRecipients.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mSubject.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mSubject.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mMessage.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mMessage.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mSendButton.setOnLongClickListener(new Button.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(getString(R.string.tts_send_button));
                return true;
            }
        });
    }

}