package uzoltan.readout.sms;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;

public class SmsOperations {

    private int DeleteSMS(ContentResolver contentResolver, int SmsId){
        try {
            String uri = "content://sms/" + SmsId;
            return contentResolver.delete(Uri.parse(uri), null, null);
        } catch (Exception e) {
            Log.v("exception","occurred");
            return 0;
        }
    }
}
