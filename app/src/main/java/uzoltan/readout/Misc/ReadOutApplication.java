package uzoltan.readout.misc;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import com.orm.SugarApp;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class ReadOutApplication extends SugarApp implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static SpeechRecognitionCommands src;

    private SharedPreferences sharedPreferences;

    public static SpeechRecognitionCommands getSRCInstance() {
        return src;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        src = new SpeechRecognitionCommands(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onTerminate();
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
