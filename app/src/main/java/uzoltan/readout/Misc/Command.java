package uzoltan.readout.misc;

import com.orm.SugarRecord;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Command extends SugarRecord {

    private String originalCommand;

    private List<String> customCommands;

    enum CommandType {

        NEW_SMS,
        READ_SMS,
        LOGIN_EMAIL,
        NEW_EMAIL,
        READ_EMAIL

    }
}
