package uzoltan.readout.misc;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import uzoltan.readout.R;
import uzoltan.readout.email.EmailFolder;
import uzoltan.readout.email.EmailFolderActivity;
import uzoltan.readout.email.EmailLoginActivity;
import uzoltan.readout.email.EmailMessage;
import uzoltan.readout.email.EmailNewActivity;
import uzoltan.readout.sms.SmsConversationListActivity;
import uzoltan.readout.sms.SmsNewActivity;

public abstract class AbstractReadOutActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final int SETTINGS_REQUEST = 114;
    private static final int PERMISSION_REQUEST = 14;
    @Getter
    protected TextToSpeech tts;
    protected SpeechRecognitionCommands src;
    protected Toolbar toolbar;
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected TextView mHeaderText;
    protected ProgressDialog progressDialog;
    private String activityTitle = "Title";
    public static final int SPEECH_RECOGNITION_REQUEST = 154;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(this.activityTitle);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initNavMenu();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        View mNavHeader = navigationView.inflateHeaderView(R.layout.nav_header);
        mHeaderText = (TextView) mNavHeader.findViewById(R.id.nav_header_email_textview);
        mHeaderText.setText(sharedPreferences.getString(SharedPreferencesUtils.KEY_SAVED_EMAIL, getString(R.string.not_logged_in_header_text)));

    }

    private boolean askForPermission(String permission) {
        if ((ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{
                            permission
//                            Manifest.permission.READ_CONTACTS,
//                            Manifest.permission.READ_SMS,
//                            Manifest.permission.RECEIVE_SMS
//                            Manifest.permission.SEND_SMS
                    },
                    PERMISSION_REQUEST);
            return false;
        } else {
            return  true;
        }
    }

    private boolean isTelephonySupport(Context context) {
        if (askForPermission(Manifest.permission.READ_SMS)) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
            if (tm.getLine1Number() == null || tm.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                return false;
            }
            PackageManager packageManager = getBaseContext().getPackageManager();
            return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT) || packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        }
        return false;
    }

    private void initNavMenu() {
        Menu menu = navigationView.getMenu();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        MenuItem loginLogoutMenuItem = menu.findItem(R.id.nav_login);
        MenuItem smsConversationsMenuItem = menu.findItem(R.id.nav_sms_group);
        MenuItem newEmailMenuItem = menu.findItem(R.id.nav_new_email);


        if ( isTelephonySupport(this) ) {
            smsConversationsMenuItem.setVisible(true);
        } else {
            smsConversationsMenuItem.setVisible(false);
        }
        if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false)) {
            loginLogoutMenuItem.setTitle(getString(R.string.nav_logout_title));
            newEmailMenuItem.setVisible(true);
            if (EmailFolder.count(EmailFolder.class) > 0) {
                List<EmailFolder> emailFolders = EmailFolder.listAll(EmailFolder.class);
                for (int i = 0; i < emailFolders.size(); i++) {
                    menu.add(R.id.nav_email_group, Menu.NONE, i + 2, emailFolders.get(i).getName());
                }
            }
        } else {
            loginLogoutMenuItem.setTitle(getString(R.string.nav_login_title));
            newEmailMenuItem.setVisible(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recreate();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReadOutApplication application = ((ReadOutApplication) getApplication());
        if (tts == null) {
            tts = new TextToSpeech(this);
        }
        src = application.getSRCInstance();
        handleTTScalls(activityTitle);
        application.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    protected void onPause() {
        if (tts != null) {
            tts.stopSpeak();
        }
        ReadOutApplication application = (ReadOutApplication) getApplication();
        application.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (tts != null) {
            tts.shutdown();
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTINGS_REQUEST) {
            recreate();
        } else if (requestCode == SPEECH_RECOGNITION_REQUEST && resultCode == RESULT_OK) {
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            int commandIndex = -2;
            for (int i = 0; i < results.size(); i++) {
                commandIndex = src.commands.indexOf(results.get(i));
                if (commandIndex != -1) {
                    break;
                }
            }

            handleSRcommand(commandIndex, results.get(0));
        }
    }

    private boolean isSameActivityInstance(Class<?> activityClass) {
        if ( activityClass.isInstance(this) ) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.tts.stopSpeak();
        if (id == R.id.nav_new_email) {
            if (isSameActivityInstance(EmailNewActivity.class)) {
                return true;
            }
            if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false)) {
                Intent intent = new Intent(AbstractReadOutActivity.this, EmailNewActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(AbstractReadOutActivity.this, R.string.login_first_toast, Toast.LENGTH_LONG).show();
                readText(getString(R.string.login_first_toast));
                Intent intent = new Intent(AbstractReadOutActivity.this, EmailLoginActivity.class);
                startActivity(intent);
            }
        } else if ( id == R.id.nav_conversation_sms ) {
            if (isSameActivityInstance(SmsConversationListActivity.class)) {
                return true;
            }
            Intent intent = new Intent(AbstractReadOutActivity.this, SmsConversationListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(AbstractReadOutActivity.this, SettingsActivity.class);
            startActivityForResult(intent, SETTINGS_REQUEST);
        } else if (id == R.id.nav_login) {
            if (isSameActivityInstance(EmailLoginActivity.class)) {
                return true;
            }
            if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false)) {
                sharedPreferences.edit()
                        .putBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false)
                        .putString(SharedPreferencesUtils.KEY_SAVED_EMAIL, "")
                        .putString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, "")
                        .commit();
                EmailFolder.deleteAll(EmailFolder.class);
                EmailMessage.deleteAll(EmailMessage.class);
            }
            Intent intent = new Intent(AbstractReadOutActivity.this, EmailLoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false)) {
                if (this instanceof EmailFolderActivity) {
                    EmailFolderActivity emailInboxActivity = (EmailFolderActivity) this;
                    emailInboxActivity.changeEmailFolder(String.valueOf(item.getTitle()));
                } else {
                    Intent intent = new Intent(AbstractReadOutActivity.this, EmailFolderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(EmailFolderActivity.EMAIL_FOLDER, item.getTitle());
                    startActivity(intent);
                }
            } else {
                Toast.makeText(AbstractReadOutActivity.this, R.string.login_first_toast, Toast.LENGTH_LONG).show();
                readText(getString(R.string.login_first_toast));
                Intent intent = new Intent(AbstractReadOutActivity.this, EmailLoginActivity.class);
                startActivity(intent);
            }
        }

        if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_VIBRATION_ASSIST, false)) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(500);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void handleSRcommand(int commandIndex, String command) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean loggedIn = sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, false);
        switch (commandIndex) {
            case -1:
                SRCommandsFragment srCommandsFragment = new SRCommandsFragment();
                Bundle speechBundle = new Bundle();
                speechBundle.putString("speech_input", command);
                srCommandsFragment.setArguments(speechBundle);
                FragmentManager fm = getSupportFragmentManager();
                srCommandsFragment.show(fm, SRCommandsFragment.TAG);
                break;
            case 0:
                if (loggedIn) {
                    Intent intent = new Intent(AbstractReadOutActivity.this, EmailNewActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(AbstractReadOutActivity.this, R.string.login_first_toast, Toast.LENGTH_LONG).show();
                    readText(getString(R.string.login_first_toast));
                }
                break;
            case 1:
                if (loggedIn) {
                    Intent intent = new Intent(AbstractReadOutActivity.this, AbstractReadOutActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(AbstractReadOutActivity.this, R.string.login_first_toast, Toast.LENGTH_LONG).show();
                    readText(getString(R.string.login_first_toast));
                }
                break;
            case 2:
                if (loggedIn) {
                } else {
                    Toast.makeText(AbstractReadOutActivity.this, R.string.login_first_toast, Toast.LENGTH_LONG).show();
                    readText(getString(R.string.login_first_toast));
                }
                break;
            case 3:
                Intent intent = new Intent(AbstractReadOutActivity.this, SmsNewActivity.class);
                startActivity(intent);
                break;
            case 4:
                break;
            case 6:
                intent = new Intent(AbstractReadOutActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            case 7:
                intent = new Intent(AbstractReadOutActivity.this, EmailLoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case 8:
                Toast.makeText(AbstractReadOutActivity.this, R.string.invalid_command_toast, Toast.LENGTH_SHORT).show();
                readText(getString(R.string.invalid_command_toast));
                break;
            default:
                srCommandsFragment = new SRCommandsFragment();
                speechBundle = new Bundle();
                speechBundle.putString("speech_input", command);
                srCommandsFragment.setArguments(speechBundle);
                fm = getSupportFragmentManager();
                srCommandsFragment.show(fm, SRCommandsFragment.TAG);
                break;
        }
    }

    public void handleTTScalls(String text) {
        readText(text);
    }

    public void readText(String toSay) {

        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        boolean isHeadsetOn = am.isWiredHeadsetOn();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isNavigationAssistOn = sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_NAVIGATION_ASSIST, false);

        boolean ignoreSilentMode;
        boolean ignoreSilentModeSMS = sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_IGNORE_SILENT_SMS, false);
        boolean ignoreSilentModeEmail = sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_IGNORE_SILENT_EMAIL, false);

        String ttsRequestID = "requestID";

        ignoreSilentMode = ignoreSilentModeEmail | ignoreSilentModeSMS;
        if (isNavigationAssistOn) {
            if (isHeadsetOn) {
                tts.speakText_flush(toSay, ttsRequestID);
            } else {
                if (ignoreSilentMode) {
                    tts.speakText_flush(toSay, ttsRequestID);
                } else {
                    switch (am.getRingerMode()) {
                        case AudioManager.RINGER_MODE_SILENT:
                            Toast.makeText(this, getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                            break;
                        case AudioManager.RINGER_MODE_VIBRATE:
                            Toast.makeText(this, getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                            break;
                        case AudioManager.RINGER_MODE_NORMAL:
                            tts.speakText_flush(toSay, ttsRequestID);
                            break;
                    }
                }
            }
        }
    }

    //checks for internet connection
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    protected void initActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferenceserences, String key) {

    }

    protected void setLocale(Locale locale) {
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }

}
