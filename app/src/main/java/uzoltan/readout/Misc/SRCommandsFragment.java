package uzoltan.readout.misc;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import uzoltan.readout.R;

public class SRCommandsFragment extends DialogFragment {

    private Context ctx;
    private TextView mSpeechInput, mSpeechCommands;

    public static final String TAG = "SRCommandsFragment";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.speech_recognition_fragment, container, false);

        setLocale();

        getDialog().setTitle(R.string.sr_command_fragment_title);

        mSpeechInput = (TextView) root.findViewById(R.id.speech_recognition_input_textview);
        mSpeechCommands = (TextView) root.findViewById(R.id.speech_recognition_commands);

        Bundle speechBundle = getArguments();
        mSpeechInput.setText(speechBundle.getString("speech_input"));
        String commandList = getString(R.string.available_voice_commands) + "\n" + getString(R.string.sr_new_email) + "\n" + getString(R.string.sr_inbox_email)
                + "\n" + getString(R.string.sr_outbox_email) + "\n" + getString(R.string.sr_new_sms) + "\n" + getString(R.string.sr_inbox_sms) + "\n" +
                getString(R.string.sr_outbox_sms) + "\n" + getString(R.string.sr_settings) + "\n" + getString(R.string.sr_login) + "\n" + getString(R.string.sr_reply);
        mSpeechCommands.setText(commandList);

        return root;
    }

    public void setLocale(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        String mLanguagePrefID = sharedPref.getString("pref_language", "1");
        Locale locale;
        switch (mLanguagePrefID) {
            case "1":
                locale = new Locale("en_US");
                break;
            case "2":
                locale = new Locale("hu");
                break;
            default:
                locale= new Locale("en_US");
        }
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }
}
