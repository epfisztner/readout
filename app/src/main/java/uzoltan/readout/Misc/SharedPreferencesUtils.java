package uzoltan.readout.misc;

public class SharedPreferencesUtils {
    public static final String KEY_LOGGED_IN = "logged_in";
    public static final String KEY_LOGIN_FAILED = "login_failed";
    public static final String KEY_LANGUAGE = "pref_language";
    public static final String KEY_SAVED_EMAIL = "saved_email";
    public static final String KEY_SAVED_PASSWORD = "saved_password";
    public static final String KEY_IMAP_HOSTNAME = "imap_hostname";
    public static final String KEY_PROVIDER_NAME = "provider_name";
    public static final String KEY_SMTP_HOSTNAME = "smtp_hostname";
    public static final String KEY_SMTP_PORT = "smtp_port";
    public static final String KEY_SPEECHRATE = "pref_speech_rate";
    public static final String KEY_AUTOMATIC_READING = "pref_automatic_reading";
    public static final String KEY_ENABLE_POPUP_SMS = "pref_enable_popup_sms";
    public static final String KEY_IGNORE_SILENT_SMS = "pref_ignore_silent_sms";
    public static final String KEY_IGNORE_SILENT_EMAIL = "pref_ignore_silent_email";
    public static final String KEY_FIRST_EMAIL_LOAD = "first_email_load";
    public static final String KEY_IS_THERE_MORE_EMAIL = "is_there_more_email";
    public static final String KEY_FETCHINBOX_MAIL_ASYNCTASK_CANCELED = "fetchinbox_mail_asynctask_canceled";
    public static final String KEY_SAVED_EMAILS_NUMBER = "pref_saved_emails_number";
    public static final String KEY_TURN_AROUND_TO_SILENCE = "pref_turn_around_to_silence";
    public static final String KEY_NAVIGATION_ASSIST = "pref_navigation_assist";
    public static final String KEY_VIBRATION_ASSIST = "pref_vibration_assist";
    public static final String KEY_SWIPE_REFRESH = "swipe_refresh";
    public static final String KEY_APP_FIRST_TIME_LAUNCHED = "app_first_time_launched";
    public static final String KEY_LANGUAGE_CHANGED = "language_changed";
    public static final String KEY_ENABLE_SMS_ACCENT_CONVERT = "pref_sms_accent_convert";
    public static final String KEY_EMAIL_FOLDER = "key_email_folder";
}
