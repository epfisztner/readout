package uzoltan.readout.misc;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class AccentObject extends SugarRecord {

    private String pureWord;

    private String accendedWord;
}
