package uzoltan.readout.misc;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import uzoltan.readout.R;

public class SettingsActivity extends AbstractReadOutActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initActivityTitle(getString(R.string.title_activity_settings));
        initToolBar();

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.settings_frame, new SettingsFragment()).commit();
    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        toolbar.setTitle(R.string.title_activity_settings);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 154 && resultCode == RESULT_OK) {
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            int commandIndex = -2;
            for (int i = 0; i < results.size(); i++) {
                commandIndex = src.commands.indexOf(results.get(i));
                if (commandIndex != -1) {
                    break;
                }
            }

            handleSRcommand(commandIndex, results.get(0));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReadOutApplication application = (ReadOutApplication) getApplication();
        application.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ReadOutApplication application = (ReadOutApplication) getApplication();
        application.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (android.R.id.home == id) {
            onBackPressed();
        }

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            } catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(SettingsActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        if (id == R.id.action_speech_settings) {
            Intent intent = new Intent();
            intent.setAction("com.android.settings.TTS_SETTINGS");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SharedPreferencesUtils.KEY_LANGUAGE) || key.equals(SharedPreferencesUtils.KEY_SPEECHRATE)) {
            Locale locale = Locale.getDefault();
            String mLanguagePrefID = sharedPreferences.getString(SharedPreferencesUtils.KEY_LANGUAGE, "1");
            src = new SpeechRecognitionCommands(getApplicationContext());
            switch (mLanguagePrefID) {
                case "1":
                    locale = Locale.US;
                    break;
                case "2":
                    locale = new Locale("hu");
                    break;
            }
            setLocale(locale);
            tts.stopSpeak();
            tts.shutdown();
            tts = new TextToSpeech(this, locale);
            sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_LANGUAGE_CHANGED, true).commit();
            recreate();
        }
        if (key.equals(SharedPreferencesUtils.KEY_TURN_AROUND_TO_SILENCE)) {
            if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_TURN_AROUND_TO_SILENCE, false)) {
                SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
                Sensor mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
                if (mGravitySensor == null) {
                    Toast.makeText(SettingsActivity.this, R.string.no_gravity_sensor, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
