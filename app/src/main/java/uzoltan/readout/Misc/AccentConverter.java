package uzoltan.readout.misc;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import uzoltan.readout.R;

public class AccentConverter {

    private Context context;
    private HashMap<String, String> stringHashMap;

    public AccentConverter(Context context) {
        this.context = context;
        stringHashMap = new HashMap<>();
    }

    public String convertText(String text) {
        if (stringHashMap.size()<1) {
            prepareResources();
        }
        Log.i("Ékezetesítés előtt", text);

        String resultText = "";
        StringTokenizer textTokenizer = new StringTokenizer(text, " \t\n\f");
        while (textTokenizer.hasMoreTokens()) {
            String word = textTokenizer.nextToken();
            StringTokenizer wordTokenizer = new StringTokenizer(word, ".!?,;:='\"_-<>{}()#*/\\+");
            if (wordTokenizer.hasMoreTokens()) {
                String pureWord = wordTokenizer.nextToken();
                String result = stringHashMap.get(pureWord.toLowerCase());
                if (result != null) {
                    resultText += word.replaceFirst("(" + pureWord + ")", result);
                    resultText += " ";
                } else {
                    resultText += word + " ";
                }
            }
        }
        Log.i("Ékezetesítés után", resultText);
        return resultText;
    }

    private void makeList() throws IOException {
        InputStream in = context.getResources().openRawResource(R.raw.ekezetek);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                StringTokenizer lineTokenizer = new StringTokenizer(line, " \t\n\f");
                if (lineTokenizer.hasMoreTokens()) {
                    String key = lineTokenizer.nextToken();
                    if (lineTokenizer.hasMoreTokens()) {
                        String value = lineTokenizer.nextToken();
                        stringHashMap.put(key, value);
                    }
                }
            }
        } finally {
            in.close();
        }
        Log.i("Ékezetesítés", "Hasmap mérete: " + stringHashMap.size());
    }

    public AccentConverter prepareResources() {
        Log.i("Ékezetesítés", "másolás megkezdve");
        try {
            makeList();
        } catch (IOException e) {
            Log.i("Ékezetesítés", "beolvasási hiba");
        }
        Log.i("Ékezetesítés", "felmásolva");
        return this;
    }

    public void releaseResources() {
        this.stringHashMap.clear();
        this.stringHashMap = null;
        this.context = null;
        System.gc();
    }

    public static String deconvertText(String text) {
        return text.replace("á", "a")
                .replace("é", "e")
                .replace("í", "i")
                .replace("ó", "o")
                .replace("ö", "o")
                .replace("ő", "o")
                .replace("ú", "u")
                .replace("ü", "u")
                .replace("ű", "u");
    }

}
