package uzoltan.readout.misc;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import uzoltan.readout.R;

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
