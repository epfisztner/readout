package uzoltan.readout.misc;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

import uzoltan.readout.asynctask.ReadLongText;

public class TextToSpeech {

    private android.speech.tts.TextToSpeech tts;

    public TextToSpeech(Context context) {
        initTTS(context, Locale.getDefault());
    }

    public TextToSpeech(Context context, Locale locale) {
        initTTS(context, locale);
    }

    private void initTTS(Context context, final Locale locale) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String speechRateID = sharedPref.getString(SharedPreferencesUtils.KEY_SPEECHRATE, "3");
        final float speechRate;
        switch (speechRateID) {
            case "1":
                speechRate = 0.5f;
                break;
            case "2":
                speechRate = 0.67f;
                break;
            case "3":
                speechRate = 1f;
                break;
            case "4":
                speechRate = 1.5f;
                break;
            case "5":
                speechRate = 2f;
                break;
            default:
                speechRate = 1f;
        }

        tts = new android.speech.tts.TextToSpeech(context, new android.speech.tts.TextToSpeech.OnInitListener() {

            @Override
            public void onInit(final int status) {
                if (status != android.speech.tts.TextToSpeech.ERROR) {
                    tts.setSpeechRate(speechRate);
                    tts.setLanguage(locale);
                }
            }
        });

    }

    public void speakText_flush(String text, String requestID) {
        text = replaceGrave(text);

        //there is a 4000 long character limit on the TextToSpeech class
        if (text.length() > 4000) {
            new ReadLongText(this).execute(text);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text, android.speech.tts.TextToSpeech.QUEUE_FLUSH, null, requestID);
            } else {
                tts.speak(text, android.speech.tts.TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    public void speakText_add(String text, String requestID) {
        text = replaceGrave(text);
        if (text.length() > 4000) {
            new ReadLongText(this).execute(text);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text, android.speech.tts.TextToSpeech.QUEUE_ADD, null, requestID);
            } else {
                tts.speak(text, android.speech.tts.TextToSpeech.QUEUE_ADD, null);
            }
        }
    }

    public void setLanguage(Locale locale) {
        tts.setLanguage(locale);
    }

    // stops any ongoing speech synthesis
    public void stopSpeak() {
        tts.stop();
    }

    // instance deleted
    public void shutdown() {
        tts.shutdown();
    }

    // most TTS engines not prepared for these charachters
    private String replaceGrave(String text) {

        text = text.replaceAll("À", "Á");
        text = text.replaceAll("à", "á");
        text = text.replaceAll("È", "É");
        text = text.replaceAll("è", "é");
        text = text.replaceAll("Ì", "Í");
        text = text.replaceAll("ì", "í");
        text = text.replaceAll("Ò", "ó");
        text = text.replaceAll("ò", "ó");
        text = text.replaceAll("Ù", "Ú");
        text = text.replaceAll("ù", "ú");
        return text;
    }
}
