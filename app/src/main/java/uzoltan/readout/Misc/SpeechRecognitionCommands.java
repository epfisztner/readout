package uzoltan.readout.misc;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import java.util.ArrayList;
import java.util.Locale;

import uzoltan.readout.R;

public class SpeechRecognitionCommands {

    public ArrayList<String> commands = new ArrayList<String>();
    private Context context;

    public SpeechRecognitionCommands(Context ctx) {

        context = ctx;
        setLocale();

        commands.clear();

        commands.add(ctx.getString(R.string.sr_new_email));
        commands.add(ctx.getString(R.string.sr_inbox_email));
        commands.add(ctx.getString(R.string.sr_outbox_email));
        commands.add(ctx.getString(R.string.sr_new_sms));
        commands.add(ctx.getString(R.string.sr_inbox_sms));
        commands.add(ctx.getString(R.string.sr_outbox_sms));
        commands.add(ctx.getString(R.string.sr_settings));
        commands.add(ctx.getString(R.string.sr_login));
        commands.add(ctx.getString(R.string.sr_reply));
    }

    public void setLocale() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String mLanguagePrefID = sharedPref.getString(SharedPreferencesUtils.KEY_LANGUAGE, "1");
        Locale locale = Locale.getDefault();
        switch (mLanguagePrefID) {
            case "1":
                locale = Locale.US;
                break;
            case "2":
                locale = new Locale("hu");
                break;
        }
        Locale.setDefault(locale);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }
}
