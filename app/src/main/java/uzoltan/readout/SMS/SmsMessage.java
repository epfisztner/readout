package uzoltan.readout.sms;

import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class SmsMessage implements Comparable<SmsMessage>, Serializable {

    private static final String DEFAULT_FORMAT = "yyyy MMM dd. hh:mm";
    private static final String NEW_SMS_FORMAT = "yyyy MMM dd. hh:mm";

    @Getter
    @Setter
    private String messageNumber;

    @Getter
    @Setter
    private String person;

    @Getter
    private String messageDate;

    @Getter
    @Setter
    private String messageContent;

    @Getter
    @Setter
    private int type;

    @Getter
    @Setter
    private int conversationId;

    @Getter
    @Setter
    private int contactId;

    @Getter
    @Setter
    private int seen;

    @Getter
    @Setter
    private int read;

    public void setMessageDate(String date) {
        this.messageDate = DateFormat.format(DEFAULT_FORMAT, Long.parseLong(date)).toString();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_FORMAT, Locale.getDefault());
            Date d = sdf.parse(this.messageDate);
            boolean fromToday = DateUtils.isToday(d.getTime());

            if (fromToday) {
                sdf.applyPattern(NEW_SMS_FORMAT);
            } else {
                sdf.applyPattern(DEFAULT_FORMAT);
            }

            this.messageDate = sdf.format(d);

        } catch (ParseException e) {
            Log.e("SMS MESSAGE DATE SET", e.getMessage());
        }
    }

    @Override
    public int compareTo(SmsMessage another) {
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_FORMAT, Locale.getDefault());
        Date thisDate = null;
        Date anotherDate = null;

        try {
            thisDate = sdf.parse(this.getMessageDate());
            anotherDate = sdf.parse(another.getMessageDate());
            return thisDate.compareTo(anotherDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
