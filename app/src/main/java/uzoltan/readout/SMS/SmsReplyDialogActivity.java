package uzoltan.readout.sms;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.R;

public class SmsReplyDialogActivity extends AbstractReadOutActivity {

    public static final String SMS_TO = "sms_to";
    private EditText mRecipients, mMessage;
    private Button mSendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms__reply_dialog);
        initActivityTitle(getString(R.string.tts_reply));

        // init views
        mRecipients = (EditText) findViewById(R.id.sms_recipients_input);
        mMessage = (EditText) findViewById(R.id.message_input);
        mSendButton = (Button) findViewById(R.id.send_button);

        mRecipients.setText(getIntent().getExtras().getString(SMS_TO));
        int length = mRecipients.length();
        mRecipients.setSelection(length);

        mSendButton.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mRecipients.getText().toString().equals("")) {
                    mRecipients.setError(getString(R.string.recipients_input_error));
                } else if (mMessage.getText().toString().equals("")) {
                    mMessage.setError(getString(R.string.message_input_error));
                } else {
                    sendSMSMessage();
                }
            }
        });
    }

    public void sendSMSMessage() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(mRecipients.getText().toString(), null, mMessage.getText().toString(), null, null);
            Toast.makeText(SmsReplyDialogActivity.this, getString(R.string.sms_sent), Toast.LENGTH_LONG).show();
            readText(getString(R.string.sms_sent));
        } catch (Exception e) {
            Toast.makeText(SmsReplyDialogActivity.this, getString(R.string.sms_sending_failed), Toast.LENGTH_LONG).show();
            readText(getString(R.string.sms_sending_failed));
            e.printStackTrace();
        } finally {
            finish();
        }
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        mRecipients.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mRecipients.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mMessage.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mMessage.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mSendButton.setOnLongClickListener(new Button.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(getString(R.string.tts_send_button));
                return true;
            }
        });
    }
}
