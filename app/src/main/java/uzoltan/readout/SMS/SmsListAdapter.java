package uzoltan.readout.sms;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uzoltan.readout.misc.RoundedImageView;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.misc.TextToSpeech;
import uzoltan.readout.R;

public class SmsListAdapter extends RecyclerView.Adapter<SmsListAdapter.SMSViewHolder> {

    private List<SmsMessage> smsList;
    private ViewGroup viewGroup;
    private TextToSpeech tts;
    private String ttsRequestID = "requestID";
    private final String DEFAULT_FORMAT = "MMM dd.HH:mm";

    public SmsListAdapter(List<SmsMessage> smsList, TextToSpeech tts) {
        this.smsList = smsList;
        this.tts = tts;
    }

    @Override
    public SmsListAdapter.SMSViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_list_row, parent, false);
        viewGroup = parent;
        return new SMSViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SMSViewHolder holder, int position) {
        final SmsMessage sms = smsList.get(position);
        final boolean isSentMessage;
        CardView cardView = null;

        if (sms.getType() == 1) { //fogadott üzenet1
            isSentMessage = false;
        } else if (sms.getType() == 2) { //elküldött
            isSentMessage = true;
        } else {
            isSentMessage = true;
        }

        final String date = sms.getMessageDate();
        LinearLayout.LayoutParams params = null;
        int backgroundColor = 0;

        if (!isSentMessage) { //fogadott üzenet
            holder.view.findViewById(R.id.sms_view_sent).setVisibility(View.GONE);
            cardView = (CardView) holder.view.findViewById(R.id.sms_card_view_recived);
            params  = (LinearLayout.LayoutParams) cardView.getLayoutParams();
            params.setMargins(0, 0, 100, 0);
            final CardView tempCardview = cardView;
            if (sms.getSeen() == 1 && sms.getRead() == 1 ) {
                backgroundColor = Color.parseColor("#BAC5FF");
            } else {
                backgroundColor = Color.parseColor("#6DDFC8");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeCardViewBackground(tempCardview, Color.parseColor("#BAC5FF"));
                    }
                },3000);
            }
        } else { //elküldött
            holder.view.findViewById(R.id.sms_view_recived).setVisibility(View.GONE);
            cardView = (CardView) holder.view.findViewById(R.id.sms_card_view_sent);
            params  = (LinearLayout.LayoutParams) cardView.getLayoutParams();
            params.setMargins(100, 0, 0, 0);
            backgroundColor = Color.parseColor("#FFFFFF");
        }

        cardView.setCardBackgroundColor(backgroundColor);
        cardView.setLayoutParams(params);

        if (!isSentMessage) {
            Bitmap bitmap = null;
            if (contactExists(viewGroup.getContext(), sms.getMessageNumber())) {
                bitmap = getPhoto(sms.getContactId(), viewGroup.getContext());
            }
            if (bitmap != null) {
                holder.phoneContactImageLeft.setImageBitmap(bitmap);
            } else {
                holder.phoneContactImageLeft.setImageResource(R.mipmap.default_avatar_tumbnail);
            }
            holder.smsDateLeft.setText(sms.getMessageDate());
            holder.smsContentLeft.setText(sms.getMessageContent());
        } else {
            holder.phoneContactImageRight.setImageResource(R.mipmap.default_avatar_tumbnail);
            holder.smsDateRight.setText(sms.getMessageDate());
            holder.smsContentRight.setText(sms.getMessageContent());
        }


        cardView.setOnClickListener(new ImageButton.OnClickListener() {

            private boolean isPlaying = false;

            @Override
            public void onClick(View v) {
                if (!isPlaying) {
                    AudioManager am = (AudioManager) viewGroup.getContext().getSystemService(Context.AUDIO_SERVICE);
                    boolean isHeadsetOn = am.isWiredHeadsetOn();

                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(viewGroup.getContext());
                    boolean ignoreSilentMode = sharedPref.getBoolean(SharedPreferencesUtils.KEY_IGNORE_SILENT_SMS, false);
                    String toSay;

                    if (isSentMessage) {
                        toSay = viewGroup.getContext().getString(R.string.tts_sent) + " " + viewGroup.getContext().getString(R.string.tts_date) + " " + date + ", " +
                                viewGroup.getContext().getString(R.string.tts_content) + sms.getMessageContent();
                    } else {
                        toSay = viewGroup.getContext().getString(R.string.tts_recived) + " " + viewGroup.getContext().getString(R.string.tts_date) + " " + date + ", " +
                                viewGroup.getContext().getString(R.string.tts_content) + sms.getMessageContent();
                    }

                    if (isHeadsetOn) {
                        isPlaying = true;
                        tts.speakText_flush(toSay, ttsRequestID);
                        Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.touch_to_stop_toast), Toast.LENGTH_LONG).show();
                    } else {
                        if (ignoreSilentMode) {
                            isPlaying = true;
                            tts.speakText_flush(toSay, ttsRequestID);
                            Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.touch_to_stop_toast), Toast.LENGTH_LONG).show();
                        } else {
                            switch (am.getRingerMode()) {
                                case AudioManager.RINGER_MODE_SILENT:
                                    Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                                    break;
                                case AudioManager.RINGER_MODE_VIBRATE:
                                    Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
                                    break;
                                case AudioManager.RINGER_MODE_NORMAL:
                                    isPlaying = true;
                                    tts.speakText_flush(toSay, ttsRequestID);
                                    Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.touch_to_stop_toast), Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }
                    }
                } else {
                    if (tts != null) {
                        Toast.makeText(viewGroup.getContext(), viewGroup.getContext().getString(R.string.touch_to_start_toast), Toast.LENGTH_LONG).show();
                        tts.stopSpeak();
                        isPlaying = false;
                    }
                }
            }
        });
    }

    private void changeCardViewBackground(CardView cardView, int color) {
        cardView.setCardBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return smsList.size();
    }

    public static class SMSViewHolder extends RecyclerView.ViewHolder {

        protected View view;
        protected RoundedImageView phoneContactImageRight;
        protected RoundedImageView phoneContactImageLeft;
        protected TextView smsDateRight;
        protected TextView smsDateLeft;
        protected TextView smsContentRight;
        protected TextView smsContentLeft;

        public SMSViewHolder(View view) {
            super(view);
            this.view = view;
            phoneContactImageLeft = (RoundedImageView) view.findViewById(R.id.contact_image_left);
            phoneContactImageRight = (RoundedImageView) view.findViewById(R.id.contact_image_right);
            smsDateRight = (TextView) view.findViewById(R.id.sms_date_right);
            smsDateLeft = (TextView) view.findViewById(R.id.sms_date_left);
            smsContentRight = (TextView) view.findViewById(R.id.message_content_right);
            smsContentLeft = (TextView) view.findViewById(R.id.message_content_left);
        }
    }

    private boolean contactExists(Context context, String number) {

        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        int indexName = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        try {
            if (cur.moveToFirst()) {
                cur.getString(indexName);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cur.close();
        }
        return false;
    }

    private Bitmap getPhoto(long contactId, Context context) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        ContentResolver cr = context.getContentResolver();
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        } else
            return BitmapFactory.decodeStream(input);
    }

}
