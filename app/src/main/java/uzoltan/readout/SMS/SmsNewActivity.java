package uzoltan.readout.sms;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.R;
import uzoltan.readout.misc.ReadOutUtilities;

public class SmsNewActivity extends AbstractReadOutActivity {

    public static final String PARTNER_NUMBER = "partnerNumber";
    private EditText mRecipients, mMessage;
    private Button mSendButton;
    private String partnerNumber = "";

    public static final int PICK_CONTACT_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms__new);
        initActivityTitle(getString(R.string.title_activity_sms__new));
        initToolBar();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                partnerNumber = "";
            } else {
                partnerNumber = extras.getString(PARTNER_NUMBER);
            }
        } else {
            partnerNumber = savedInstanceState.getString(PARTNER_NUMBER);
        }

        mRecipients = (EditText) findViewById(R.id.sms_recipients_input);
        mRecipients.setText(partnerNumber, TextView.BufferType.EDITABLE);
        mMessage = (EditText) findViewById(R.id.message_input);
        mSendButton = (Button) findViewById(R.id.send_button);

        mSendButton.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mRecipients.getText().toString().equals("")) {
                    mRecipients.setError(getString(R.string.recipients_input_error));
                } else if (mMessage.getText().toString().equals("")) {
                    mMessage.setError(getString(R.string.message_input_error));
                } else {
                    sendSMSMessage();
                }

            }
        });
    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_sms__new));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void sendSMSMessage() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(mRecipients.getText().toString(), null, mMessage.getText().toString(), null, null);
            Toast.makeText(SmsNewActivity.this, getString(R.string.sms_sent), Toast.LENGTH_LONG).show();
            readText(getString(R.string.sms_sent));
            finish();
        } catch (Exception e) {
            Toast.makeText(SmsNewActivity.this, getString(R.string.sms_sending_failed), Toast.LENGTH_LONG).show();
            readText(getString(R.string.sms_sending_failed));
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {

            // Get the URI that points to the selected contact
            final Uri contactUri = data.getData();
            // We only need the NUMBER column, because there will be only one row in the result
            final String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

            // Perform the query on the contact to get the NUMBER column
            // We don't need a selection or sort order (there's only one result for the given URI)
            // The query() method should be called from a separate thread to avoid blocking the app's UI thread.

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Cursor cursor = getContentResolver()
                            .query(contactUri, projection, null, null, null);

                    cursor.moveToFirst();

                    // Retrieve the phone number from the NUMBER column
                    int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    final String number = cursor.getString(column);
                    cursor.close();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mRecipients.getText().toString().isEmpty()) {
                                mRecipients.setText(number);
                            } else {
                                mRecipients.append(", " + number);
                            }
                            int position = mRecipients.getText().length();
                            mRecipients.setSelection(position);
                        }
                    });
                }
            }).run();
        }
}

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sms__new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_sms_contacts) {
            Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));

            // Show user only contacts with phone numbers
            pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
        }

        if(id == R.id.action_speech_recognition){
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            }
            catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(SmsNewActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        mRecipients.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mRecipients.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mMessage.setOnLongClickListener(new EditText.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                android.support.design.widget.TextInputLayout parent = (android.support.design.widget.TextInputLayout) mMessage.getParent();
                if (parent.getHint().toString() != null) {
                    readText(parent.getHint().toString());
                }
                return true;
            }
        });

        mSendButton.setOnLongClickListener(new Button.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                readText(getString(R.string.tts_send_button));
                return true;
            }
        });
    }

}
