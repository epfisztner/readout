package uzoltan.readout.sms;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uzoltan.readout.R;
import uzoltan.readout.asynctask.FetchConversations;
import uzoltan.readout.asynctask.FetchConversationsInterface;
import uzoltan.readout.misc.AbstractReadOutActivity;

public class SmsConversationListActivity extends AbstractReadOutActivity implements FetchConversationsInterface {

    private static final int PERMISSION_REQUEST = 14;
    private RecyclerView recList;
    private SensorManager mSensorManager;
    private Sensor mGravitySensor;
    private FetchConversations mAsyncTask = null;
    private LinearLayoutManager llm;
    public List<SmsConversation> loadedMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_conversation_list);
        initActivityTitle(getString(R.string.title_activity_sms__conversation));
        initToolBar();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        //setting up the recyclerview
        recList = (RecyclerView) findViewById(R.id.sms_conversation_list);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        checkPermission();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.new_conversation_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(SmsConversationListActivity.this, SmsNewActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//            if (!Telephony.Sms.getDefaultSmsPackage(this).equals(this.getClass().getPackage())) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(SmsConversationListActivity.this);
//                builder.setMessage("This app is not set as your default messaging app. Do you want to set it as default?")
//                        .setCancelable(false)
//                        .setTitle("Alert!")
//                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        })
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @TargetApi(19)
//                            public void onClick(DialogInterface dialog, int id) {
//                                Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
//                                intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, getPackageName());
//                                startActivity(intent);
//                            }
//                        });
//                builder.show();
//            }
//        }
        checkPermission();
        progressDialog = ProgressDialog.show(this, null, this.getString(R.string.fetching_sms_dialog), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new ProgressDialog.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                mAsyncTask.cancel(true);
                Toast.makeText(SmsConversationListActivity.this, R.string.sms_fetching_canceled_toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAsyncTask != null) {
            mAsyncTask.cancel(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAsyncTask != null) {
            mAsyncTask.cancel(true);
            mAsyncTask = null;
        }
    }

    private void loadSmsConversations() {
        if (mAsyncTask == null || mAsyncTask.getStatus() != AsyncTask.Status.PENDING) {
            mAsyncTask = new FetchConversations(this);
        }

        loadedMessages = new ArrayList<>();

        mAsyncTask.execute();

        recList.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

//                visibleItemCount = recyclerView.getChildCount();
//                totalItemCount =recyclerView.getItemCount();
//                firstVisibleItem = recyclerView.findFirstVisibleItemPosition();
//
//                logic decides if we need to load more emailFolders or not
//                if (loading) {
//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                    Boolean canceled = sharedPref.getBoolean(SharedPreferencesUtils.KEY_FETCHINBOX_MESSAGES_ASYNCTASK_CANCELED, false);
//                    if (canceled) {
//                        loading = false;
//                    }
//                    sharedPref.edit().putBoolean(SharedPreferencesUtils.KEY_FETCHINBOX_MESSAGES_ASYNCTASK_CANCELED, false).apply();
//                }
//
//                if (!loading && isThereMore && totalItemCount == (firstVisibleItem + visibleItemCount) && visibleItemCount > 0) {
//                    loading = true;
//                    mAsyncTask = new FetchMessages(this, startIndex, offset, 1);
//                    mAsyncTask.execute();
//                }
                }
        });
    }

    @Override
    public void AsyncTaskFinish(List<SmsConversation> smsConversations) {

        loadedMessages.clear();

        loadedMessages.addAll(smsConversations);

        progressDialog.dismiss();

        if (loadedMessages.size() > 0) {
            SmsConversationListAdapter mAdapter = new SmsConversationListAdapter(loadedMessages, tts);
            mAdapter.setOnItemClickListener(new SmsConversationListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Intent intent = new Intent(SmsConversationListActivity.this, SmsConversationActivity.class);
                    intent.putExtra(SmsConversationActivity.SMS_PHONE, loadedMessages.get(position).getPartnerPhoneNumber());
                    intent.putExtra(SmsConversationActivity.SMS_TRHEAD_ID, loadedMessages.get(position).getThreadId());
                    startActivity(intent);
                }
            });
            recList.setAdapter(mAdapter);

//            startIndex = loadedMessages.size();
//
//            if (smsConversations.size() < offset) {
//                isThereMore = false;
//                Toast.makeText(this, R.string.last_sms_toast, Toast.LENGTH_LONG).show();
//                readText(getString(R.string.last_sms_toast));
//            }
            //recList.scrollToPosition(mAdapter.getItemCount() - 1);
//            loading = false;
        }
    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS},
                    PERMISSION_REQUEST);
        } else {
            loadSmsConversations();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recreate();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
