package uzoltan.readout.sms;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.misc.ReadOutUtilities;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.R;

public class SmsIncomingDialogActivity extends AbstractReadOutActivity implements SensorEventListener {

    public static final String SMS_SENDER = "sms_sender";
    public static final String SMS_CONTENT = "sms_content";

    private TextView mSender, mContent, mTitle;
    private Button mClose, mReply;
    private ImageButton mTTSSpeaker;
    private String sender, content;
    private boolean isFaceDown;
    private SensorManager mSensorManager;
    private Sensor mGravitySensor;
    private String smsSender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Window window = getWindow();
        window.addFlags(android.view.WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setContentView(R.layout.activity_sms__incoming_dialog);
        super.onCreate(savedInstanceState);
        initActivityTitle(getString(R.string.new_sms_title));
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        smsSender = extras.getString(SMS_SENDER, "");


        //init views
        mTitle = (TextView) findViewById(R.id.new_incoming_sms_title);
        mTitle.setText(R.string.new_sms_title);
        mSender = (TextView) findViewById(R.id.new_incoming_sms_sender);
        mContent = (TextView) findViewById(R.id.new_incoming_sms_content);
        mClose = (Button) findViewById(R.id.btnClose);
        mReply = (Button) findViewById(R.id.btnReply);
        mTTSSpeaker = (ImageButton) findViewById(R.id.tts_speaker);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        String contact = ReadOutUtilities.getContactName(SmsIncomingDialogActivity.this, extras.getString(SMS_SENDER));
        sender = getString(R.string.from_text) + " " + contact;
        content = getString(R.string.message_text) + "\n\n" + extras.getString(SMS_CONTENT);
        mSender.setText(sender);
        mContent.setText(content);

        mContent.setMovementMethod(new ScrollingMovementMethod());

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmsIncomingDialogActivity.this, SmsReplyDialogActivity.class);
                intent.putExtra(SmsReplyDialogActivity.SMS_TO, smsSender);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGravitySensor != null) {
            mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.unregisterListener(this);
    }

    @Override
    public void handleTTScalls(String title) {
        super.handleTTScalls(title);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isNavigationAssistOn = sharedPref.getBoolean(SharedPreferencesUtils.KEY_NAVIGATION_ASSIST, false);
        boolean isAutomaticReadingOn = sharedPref.getBoolean(SharedPreferencesUtils.KEY_AUTOMATIC_READING, false);
        if (isAutomaticReadingOn) {
            String toSay = sender + content;
            readText(toSay);
        }

        mTTSSpeaker.setOnClickListener(new ImageButton.OnClickListener() {

            @Override
            public void onClick(View v) {
                String toSay = sender + ", " + content;
                readText(toSay);
            }
        });

        mTTSSpeaker.setOnLongClickListener(new ImageButton.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                tts.stopSpeak();
                return true;
            }
        });

        if (isNavigationAssistOn) {

            mClose.setOnLongClickListener(new Button.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    readText(mClose.getText().toString());
                    return true;
                }
            });

            mReply.setOnLongClickListener(new Button.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    readText(mReply.getText().toString());
                    return true;
                }
            });
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        final float factor = 0.85F; //recommended value is between 0.7-0.95.
        if (event.sensor == mGravitySensor) {
            boolean nowDown = event.values[2] < -SensorManager.GRAVITY_EARTH * factor;
            if (nowDown != isFaceDown) {
                isFaceDown = nowDown;  //update our global variable
            }

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean enableTurnOver = sharedPref.getBoolean(SharedPreferencesUtils.KEY_TURN_AROUND_TO_SILENCE, false);
            if (enableTurnOver) {
                if (isFaceDown)
                    tts.stopSpeak();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //needed to implement SensorEventListener
    }
}
