package uzoltan.readout.sms;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SmsConversation implements Serializable {

    private int threadId;
    private int messageCount;
    private String snippet;
    private String partnerPhoneNumber;
    private int contactId;
    private List<SmsMessage> messageList;
}
