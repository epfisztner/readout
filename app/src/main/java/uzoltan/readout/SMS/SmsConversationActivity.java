package uzoltan.readout.sms;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uzoltan.readout.R;
import uzoltan.readout.asynctask.FetchMessagesByConversation;
import uzoltan.readout.asynctask.FetchMessagesByConversationInterface;
import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.misc.ReadOutUtilities;
import uzoltan.readout.misc.SharedPreferencesUtils;

public class SmsConversationActivity extends AbstractReadOutActivity implements FetchMessagesByConversationInterface, SensorEventListener {

    public static final String SMS_PHONE = "smsPhone";
    public static final String SMS_TRHEAD_ID = "smsThreadId";
    public static final int NEW_SMS_REQUEST = 1114;
    private RecyclerView recList;
    private LinearLayoutManager llm;
    private boolean isFaceDown;
    private SensorManager mSensorManager;
    private Sensor mGravitySensor;
    private List<SmsMessage> loadedMessages = new ArrayList<SmsMessage>();
    private FetchMessagesByConversation loadMessageTask = null;
    private String phoneNumber;
    private Integer threadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_conversation);
        initActivityTitle(getString(R.string.title_activity_sms__inbox));

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                phoneNumber = null;
                threadId = null;
            } else {
                phoneNumber = extras.getString(SMS_PHONE);
                threadId = extras.getInt(SMS_TRHEAD_ID);
            }
        } else {
            phoneNumber = savedInstanceState.getString(SMS_PHONE);
            threadId = savedInstanceState.getInt(SMS_TRHEAD_ID);
        }


        initToolBar();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        //setting up the recyclerview
        recList = (RecyclerView) findViewById(R.id.sms_list);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.new_sms_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(SmsConversationActivity.this, SmsNewActivity.class);
                intent.putExtra(SmsNewActivity.PARTNER_NUMBER, phoneNumber);
                startActivityForResult(intent, NEW_SMS_REQUEST);
            }
        });

    }

    @Override
    protected void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(ReadOutUtilities.getContactName(this, phoneNumber));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGravitySensor != null) {
            mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        loadMessageTask = new FetchMessagesByConversation(this,threadId);
        loadMessageTask.execute((Void[]) null);
        progressDialog = ProgressDialog.show(this, null, this.getString(R.string.fetching_sms_dialog), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new ProgressDialog.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                loadMessageTask.cancel(true);
                Toast.makeText(SmsConversationActivity.this, R.string.sms_fetching_canceled_toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sms__inbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, 154);
            } catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(SmsConversationActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        final float factor = 0.85F; //recommended value is between 0.7-0.95.
        if (event.sensor == mGravitySensor) {
            boolean nowDown = event.values[2] < -SensorManager.GRAVITY_EARTH * factor;
            if (nowDown != isFaceDown) {
                isFaceDown = nowDown;  //update our global variable
            }

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean enableTurnOver = sharedPref.getBoolean(SharedPreferencesUtils.KEY_TURN_AROUND_TO_SILENCE, false);
            if (enableTurnOver) {
                if (isFaceDown)
                    tts.stopSpeak();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //needed to implement SensorEventListener
    }

    @Override
    public void AsyncTaskFinish(SmsConversation smsConversation) {

        loadedMessages.clear();

        loadedMessages.addAll(smsConversation.getMessageList());

        progressDialog.dismiss();

        if (loadedMessages.size() > 0) {
            recList.setAdapter(new SmsListAdapter(loadedMessages, tts));

//            startIndex = loadedMessages.size();
//
//            if (smsConversations.size() < offset) {
//                isThereMore = false;
//                Toast.makeText(this, R.string.last_sms_toast, Toast.LENGTH_LONG).show();
//                readText(getString(R.string.last_sms_toast));
//            }
            //recList.scrollToPosition(mAdapter.getItemCount() - 1);
//            loading = false;
        }
    }

}
