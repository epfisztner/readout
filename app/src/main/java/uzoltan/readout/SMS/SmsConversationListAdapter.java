package uzoltan.readout.sms;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uzoltan.readout.misc.ReadOutUtilities;
import uzoltan.readout.misc.TextToSpeech;
import uzoltan.readout.R;

public class SmsConversationListAdapter extends RecyclerView.Adapter<SmsConversationListAdapter.SmsConversationViewHolder> {

    private List<SmsConversation> smsConversations;
    private ViewGroup context;
//    private TextToSpeech tts;
//    private String ttsRequestID = "requestID";
    private OnItemClickListener onItemClickListener;

    public SmsConversationListAdapter(List<SmsConversation> smsConversations, TextToSpeech tts) {
        this.smsConversations = smsConversations;
//        this.tts = tts;
    }

    @Override
    public SmsConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.sms_conversation_row, parent, false);
        context = parent;
        return new SmsConversationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SmsConversationViewHolder holder, int position) {
        final SmsConversation smsConversation = smsConversations.get(position);
        if (smsConversation.getMessageCount() > 0) {
//            final String contact = ReadOutUtilities.getContactName(context.getContext(), smsConversation.getPartnerPhoneNumber());
//            holder.vPhoneContact.setText(contact);
//            holder.vTTSSpeaker.setImageResource(R.drawable.ic_volume_up_24dp);
//            holder.vSMSDate.setText(lastSmsMessage.getMessageDate());
            holder.vSMSContent.setText(smsConversation.getSnippet());
//TODO törölni ezeket a sorokat miután refactoráltam mindent
//            holder.vTTSSpeaker.setOnClickListener(new ImageButton.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    AudioManager am = (AudioManager) context.getContext().getSystemService(Context.AUDIO_SERVICE);
//                    boolean isHeadsetOn = am.isWiredHeadsetOn();
//
//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context.getContext());
//                    boolean ignoreSilentMode = sharedPref.getBoolean(SharedPreferencesUtils.KEY_IGNORE_SILENT_SMS, false);
//                    String toSay;
//
//                    toSay = context.getContext().getString(R.string.tts_conversation) + contact + ", " +
//                            context.getContext().getString(R.string.tts_content) + lastSmsMessage.getMessageContent();
//
//                    if (isHeadsetOn) {
//                        tts.speakText_flush(toSay, ttsRequestID);
//                        Toast.makeText(context.getContext(), context.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
//                    } else {
//                        if (ignoreSilentMode) {
//                            tts.speakText_flush(toSay, ttsRequestID);
//                            Toast.makeText(context.getContext(), context.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
//                        } else {
//                            switch (am.getRingerMode()) {
//                                case AudioManager.RINGER_MODE_SILENT:
//                                    Toast.makeText(context.getContext(), context.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
//                                    break;
//                                case AudioManager.RINGER_MODE_VIBRATE:
//                                    Toast.makeText(context.getContext(), context.getContext().getString(R.string.unmute_toast), Toast.LENGTH_LONG).show();
//                                    break;
//                                case AudioManager.RINGER_MODE_NORMAL:
//                                    tts.speakText_flush(toSay, ttsRequestID);
//                                    Toast.makeText(context.getContext(), context.getContext().getString(R.string.hold_down_to_stop_toast), Toast.LENGTH_LONG).show();
//                                    break;
//                            }
//                        }
//                    }
//                }
//            });

//            holder.vTTSSpeaker.setOnLongClickListener(new ImageButton.OnLongClickListener() {
//
//                @Override
//                public boolean onLongClick(View v) {
//
//                    if (tts != null) {
//                        tts.stopSpeak();
//                    }
//                    return true;
//                }
//            });
        }
    }

    @Override
    public int getItemCount() {
        return smsConversations.size();
    }

    public class SmsConversationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected CardView vCardView;
        protected TextView vPhoneContact;
//        protected ImageButton vTTSSpeaker;
        protected TextView vSMSDate;
        protected TextView vSMSContent;

        public SmsConversationViewHolder(View view) {
            super(view);
            vCardView = (CardView) view.findViewById(R.id.sms_conversation_card_view);
            vPhoneContact = (TextView) view.findViewById(R.id.partner_name);
//            vTTSSpeaker = (ImageButton) view.findViewById(R.id.tts_speaker);
            vSMSDate = (TextView) view.findViewById(R.id.last_sms_date);
            vSMSContent = (TextView) view.findViewById(R.id.last_message_content);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(view,getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.onItemClickListener = mItemClickListener;
    }

}
