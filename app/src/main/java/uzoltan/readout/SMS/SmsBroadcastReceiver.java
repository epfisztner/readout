package uzoltan.readout.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import lombok.NoArgsConstructor;
import uzoltan.readout.misc.SharedPreferencesUtils;

@NoArgsConstructor
public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean enablePopup = sharedPref.getBoolean(SharedPreferencesUtils.KEY_ENABLE_POPUP_SMS, true);

        if (enablePopup) {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    String format = bundle.getString("format");

                    final SmsMessage[] messages = new SmsMessage[pdus.length];

                    for (int i = 0; i < pdus.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                        } else {
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }
                    }

                    String sender = messages[0].getOriginatingAddress();
                    String content = messages[0].getMessageBody();

                    if (messages.length > -1) {
                        startDialogActivity(sender, content);
                    }
                }
            }
        }
    }

    public void startDialogActivity(String sender, String content) {
        Intent SmsPopup = new Intent(context, SmsIncomingDialogActivity.class);
        SmsPopup.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle SmsData = new Bundle();
        SmsData.putString(SmsIncomingDialogActivity.SMS_SENDER, sender);
        SmsData.putString(SmsIncomingDialogActivity.SMS_CONTENT, content);
        SmsPopup.putExtras(SmsData);
        context.startActivity(SmsPopup);
    }
}
