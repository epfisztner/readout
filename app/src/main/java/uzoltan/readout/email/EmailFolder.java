package uzoltan.readout.email;

import com.orm.SugarRecord;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Setter
@Getter
public class EmailFolder extends SugarRecord {

    private String name;

}
