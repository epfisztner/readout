package uzoltan.readout.email;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;

import lombok.Setter;
import uzoltan.readout.R;
import uzoltan.readout.asynctask.FetchEmails;
import uzoltan.readout.asynctask.FetchEmailsInterface;
import uzoltan.readout.misc.ReadOutUtilities;
import uzoltan.readout.misc.SharedPreferencesUtils;

public class EmailFolderFragment extends Fragment implements FetchEmailsInterface, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "EmailFolderFragment";
    private static final String EMAIL_FOLDER = "emailFolder";
    private FetchEmails mAsyncTask;
    private RecyclerView recList;
    private LinearLayoutManager llm;
    private SwipeRefreshLayout srl;
    private String email, password, imapHostName;
    private boolean isThereMore = true, loading, swipeRefresh = false;
    private LinkedList<EmailMessage> loadedEmails = new LinkedList<>();
    @Setter
    private OnEmailFolderFailedListener onEmailFolderFailedListener = null;
    private String emailFolder;
    private EmailFolderActivity emailInboxActivity = null;

    private int firstVisibleItem, visibleItemCount, totalItemCount, startIndex = 0, offset = 10;

    private SharedPreferences sharedPref;
    private ProgressDialog progressDialog;

    public EmailFolderFragment() {

    }

    public static EmailFolderFragment newInstance(String emailFolder) {
        EmailFolderFragment fragment = new EmailFolderFragment();
        Bundle args = new Bundle();
        args.putString(EMAIL_FOLDER, emailFolder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        emailInboxActivity = (EmailFolderActivity) getActivity();
        if (getArguments() != null) {
            emailFolder = getArguments().getString(EMAIL_FOLDER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_email_folder, container, false);
        //getting the intent info
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        email = sharedPref.getString(SharedPreferencesUtils.KEY_SAVED_EMAIL, "");
        password = sharedPref.getString(SharedPreferencesUtils.KEY_SAVED_PASSWORD, "");
        imapHostName = sharedPref.getString(SharedPreferencesUtils.KEY_IMAP_HOSTNAME, "");

        //setting up the recyclerview
        recList = (RecyclerView) view.findViewById(R.id.email_list);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        srl = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_email_list);
        srl.setOnRefreshListener(this);

        loading = true;
        mAsyncTask = new FetchEmails(getActivity(), email, password, imapHostName, startIndex, offset);
        mAsyncTask.setDelegate(this);
        mAsyncTask.execute(emailFolder);

        progressDialog = ProgressDialog.show(getActivity(), null, this.getString(R.string.fetching_email_dialog), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new ProgressDialog.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                mAsyncTask.cancel(true);
                //Toast.makeText(getActivity(), R.string.email_fetching_canceled_toast, Toast.LENGTH_LONG).show();
            }
        });

//        recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                visibleItemCount = llm.getChildCount();
//                totalItemCount = llm.getItemCount();
//                firstVisibleItem = llm.findFirstVisibleItemPosition();
//
//                if (loading) {
//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    Boolean canceled = sharedPref.getBoolean(SharedPreferencesUtils.KEY_FETCHINBOX_MESSAGES_ASYNCTASK_CANCELED, false);
//                    if (canceled) {
//                        loading = false;
//                    }
//                    sharedPref.edit().putBoolean(SharedPreferencesUtils.KEY_FETCHINBOX_MESSAGES_ASYNCTASK_CANCELED, false).apply();
//                }
//
//                if (!loading && isThereMore && totalItemCount == (firstVisibleItem + visibleItemCount) && visibleItemCount > 0) {
//                    if (ReadOutUtilities.isOnline(getActivity())) {
//                        loading = true;
//                        mAsyncTask = new FetchEmails(getActivity(), email, password, imapHostName, startIndex, offset);
//                        mAsyncTask.setDelegate(EmailFolderFragment.this);
//                        mAsyncTask.execute(emailFolder);
//                    } else {
//                        if ((int) EmailMessage.count(EmailMessage.class, "message_folder = ?", new String[]{emailFolder}) <= startIndex) {
//                            recList.scrollToPosition(startIndex + offset - 5);
//                            Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
//                            emailInboxActivity.readText(getString(R.string.no_internet_connection));
//                        } else {
//                            loading = true;
//                            mAsyncTask = new FetchEmails(getActivity(), email, password, imapHostName, startIndex, offset);
//                            mAsyncTask.setDelegate(EmailFolderFragment.this);
//                            mAsyncTask.execute(emailFolder);
//                        }
//                    }
//                }
//            }
//        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EmailFolderActivity emailFolderActivity = (EmailFolderActivity) getActivity();
        emailFolderActivity.handleTTScalls(emailFolder);
//        mAsyncTask = new FetchEmails(getActivity(), email, password, imapHostName, startIndex, offset);
//        mAsyncTask.setDelegate(this);
//        mAsyncTask.execute(emailFolder);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAsyncTask.cancel(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEmailFolderFailedListener) {
            onEmailFolderFailedListener = (OnEmailFolderFailedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEmailFolderFailedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onEmailFolderFailedListener = null;
    }

    @Override
    public void AsyncTaskFinish(final ArrayList<EmailMessage> emails) {
        progressDialog.dismiss();
        if (sharedPref.getBoolean(SharedPreferencesUtils.KEY_LOGIN_FAILED, false)) {
            Toast.makeText(getActivity(), R.string.login_failed_toast, Toast.LENGTH_LONG).show();
            emailInboxActivity.readText(getString(R.string.login_failed_toast));
            if (onEmailFolderFailedListener != null) {
                onEmailFolderFailedListener.finishActivity();
            }
        } else {
            if (emails != null && emails.size() > 0)
//                if (swipeRefresh) {
//                    swipeRefresh = false;
//
//                    if (!(emails.get(0).getUID() == loadedEmails.get(0).getUID())) {
                        loadedEmails.clear();
                        loadedEmails.addAll(emails);
//                    }

                    EmailListAdapter mAdapter = new EmailListAdapter(loadedEmails, ((EmailFolderActivity) getActivity()).getTts());
                    recList.setAdapter(mAdapter);

//                } else {
//                    loadedEmails.addAll(emails);
//                    EmailListAdapter mAdapter = new EmailListAdapter(loadedEmails, ((EmailFolderActivity) getActivity()).getTts());
//                    recList.setAdapter(mAdapter);
//
//                    isThereMore = sharedPref.getBoolean(SharedPreferencesUtils.KEY_IS_THERE_MORE_EMAIL, true);
//                    if (!isThereMore) {
//                        Toast.makeText(getActivity(), getString(R.string.last_email_toast), Toast.LENGTH_LONG).show();
//                        emailInboxActivity.readText(getString(R.string.last_email_toast));
//                    }
//
//                    boolean firstLoad = sharedPref.getBoolean(SharedPreferencesUtils.KEY_FIRST_EMAIL_LOAD, false);
//                    if (!firstLoad) {
//                        recList.scrollToPosition(mAdapter.getItemCount() - (offset + 1));
//                    } else {
//                        recList.scrollToPosition(0);
//                    }
//                    sharedPref.edit().putBoolean(SharedPreferencesUtils.KEY_FIRST_EMAIL_LOAD, false).apply();
//                }
//
//            startIndex = loadedEmails.size();
            loading = false;
            srl.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        if (ReadOutUtilities.isOnline(getActivity())) {
            srl.setRefreshing(true);
            swipeRefresh = true;
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
            sharedPref.edit().putBoolean(SharedPreferencesUtils.KEY_SWIPE_REFRESH, true).apply();

            loading = true;
            mAsyncTask = new FetchEmails(getActivity(), email, password, imapHostName, 0, offset);
            mAsyncTask.setDelegate(this);
            mAsyncTask.execute(emailFolder);
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
            emailInboxActivity.readText(getString(R.string.no_internet_connection));
            srl.setRefreshing(false);
        }
    }

    public interface OnEmailFolderFailedListener {
        void finishActivity();
    }
}
