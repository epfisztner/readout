package uzoltan.readout.email;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import uzoltan.readout.R;
import uzoltan.readout.misc.AbstractReadOutActivity;
import uzoltan.readout.misc.SharedPreferencesUtils;

public class EmailFolderActivity extends AbstractReadOutActivity implements SensorEventListener, EmailFolderFragment.OnEmailFolderFailedListener {

    public static final String EMAIL_FOLDER = "emailFolder";

    private SensorManager mSensorManager;
    private Sensor mGravitySensor;
    private boolean isFaceDown = false;
    private String emailFolder = "";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email__inbox);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Bundle bundle = getIntent().getExtras();
        EmailFolder emailFolderObject = EmailFolder.first(EmailFolder.class);
        if (savedInstanceState != null) {
            emailFolder = savedInstanceState.getString(EMAIL_FOLDER);
        } else if (bundle != null) {
            emailFolder = bundle.getString(EMAIL_FOLDER);
        } else {
            emailFolder = emailFolderObject != null ? emailFolderObject.getName() : "";
        }
        if (emailFolder != null && emailFolder.equals("") && emailFolderObject != null) {
            emailFolder = emailFolderObject.getName();
        } else {
            emailFolder = sharedPreferences.getString(SharedPreferencesUtils.KEY_EMAIL_FOLDER, "");
            if (emailFolder.equals("")) {
                finish();
            }
        }
        initActivityTitle(emailFolder);
        initToolBar();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        EmailFolderFragment emailFolderFragment = EmailFolderFragment.newInstance(emailFolder);
        emailFolderFragment.setOnEmailFolderFailedListener(this);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(R.id.email_folder_container, emailFolderFragment, EmailFolderFragment.TAG);
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGravitySensor != null) {
            mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        this.handleTTScalls(emailFolder);
        changeEmailFolder(sharedPreferences.getString(SharedPreferencesUtils.KEY_EMAIL_FOLDER, emailFolder));
    }

    @Override
    protected void onPause() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }
        sharedPreferences.edit().putString(SharedPreferencesUtils.KEY_EMAIL_FOLDER, emailFolder).commit();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.email__inbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handling action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_speech_recognition) {
            try {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.asr_prompt);
                startActivityForResult(i, SPEECH_RECOGNITION_REQUEST);
            } catch (ActivityNotFoundException activityNotFound) {
                Toast.makeText(EmailFolderActivity.this, R.string.no_asr_module_found, Toast.LENGTH_LONG).show();
                readText(getString(R.string.no_asr_module_found));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        final float factor = 0.85F; ////recommended value is between 0.7-0.95. this variable says how much the turn around must be completed to be registered. think of it as a % value from 0-100%
        if (event.sensor == mGravitySensor) {
            boolean nowDown = event.values[2] < -SensorManager.GRAVITY_EARTH * factor;
            if (nowDown != isFaceDown) {
                isFaceDown = nowDown;  //update our global variable
            }

            Boolean enableTurnOver = sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_TURN_AROUND_TO_SILENCE, false);
            if (enableTurnOver) {
                if (isFaceDown)
                    tts.stopSpeak();
            }
        }
    }

    public void changeEmailFolder(String emailFolder) {
        toolbar.setTitle(emailFolder);
        EmailFolderFragment emailFolderFragment = EmailFolderFragment.newInstance(emailFolder);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.email_folder_container, emailFolderFragment, EmailFolderFragment.TAG);
        ft.commit();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //needed to implement SensorEventListener
    }

    @Override
    public void finishActivity() {
        finish();
    }
}
