package uzoltan.readout.asynctask;

import uzoltan.readout.sms.SmsConversation;

public interface FetchMessagesByConversationInterface {

    void AsyncTaskFinish(SmsConversation smsConversation);
}
