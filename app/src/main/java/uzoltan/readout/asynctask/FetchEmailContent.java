package uzoltan.readout.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

import uzoltan.readout.R;

public class FetchEmailContent extends AsyncTask<Void, Integer, String> {

    public FetchEmailContentInterface delegate = null;
    private Context ctx;
    private long uid;
    private String email, password, imapHostName;
    private ProgressDialog progress;
    private boolean textIsHtml = false;
    public String content = "";

    public FetchEmailContent(Context ctx, String email, String password, String imapHostName, long uid) {
        this.ctx = ctx;
        this.email = email;
        this.password = password;
        this.imapHostName = imapHostName;
        this.uid = uid;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = ProgressDialog.show(ctx, null, ctx.getString(R.string.fetching_email_content_dialog), true);
        progress.setCancelable(true);
        progress.setOnCancelListener(new ProgressDialog.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
                Toast.makeText(ctx, R.string.email_fetching_canceled_toast, Toast.LENGTH_LONG).show();
            }
        });
    }

    protected String getText(Part p) throws
            MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            String s = (String) p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp);
                    textIsHtml = false;
                    //continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    textIsHtml = true;
                    if (s != null)
                        return s;
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }
        return null;
    }

    @Override
    protected String doInBackground(Void... params) {

        try {
            Properties properties = new Properties();
            properties.put("mail.store.protocol", "imaps");
            properties.put("mail.imap.ssl.trust", "*");

            Session emailSession = Session.getDefaultInstance(properties);
            Store store = emailSession.getStore("imaps");
            store.connect(imapHostName, email, password);

            Folder emailFolder = store.getFolder("INBOX");
            UIDFolder uf = (UIDFolder)emailFolder;

            emailFolder.open(Folder.READ_WRITE);

            Message message = uf.getMessageByUID(uid);
            Object msgContent = message.getContent();
            if (msgContent instanceof Multipart) {

                Multipart multipart = (Multipart) msgContent;

                for (int k = 0; k < multipart.getCount(); k++) {

                    BodyPart bodyPart = multipart.getBodyPart(k);

                    String textContent = getText((Part) bodyPart);

                    if (textContent != null && textIsHtml)
                        content = textContent;

                    String disposition = bodyPart.getDisposition();

                    if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) {
                        DataHandler handler = bodyPart.getDataHandler();
                        content = content + "\n<br>" + ctx.getString(R.string.email_attachment_text) + "\n<br>" + handler.getName();
                    } else if(content.isEmpty()) {
                        content = bodyPart.getContent().toString();
                    }
                }
            } else
                content = message.getContent().toString();
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }

    @Override
    protected void onPostExecute(String emailContent) {
        super.onPostExecute(emailContent);
        progress.dismiss();

        delegate.AsyncTaskFinish(emailContent);
    }
}