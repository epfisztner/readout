package uzoltan.readout.asynctask;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Telephony;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import uzoltan.readout.misc.AccentConverter;
import uzoltan.readout.misc.SharedPreferencesUtils;
import uzoltan.readout.sms.SmsConversation;
import uzoltan.readout.sms.SmsMessage;

public class FetchMessagesByConversation extends AsyncTask<Void, Integer, SmsConversation> {

    SmsConversation smsConversation;
    public FetchMessagesByConversationInterface delegate = null;
    private Context context;
    private AccentConverter accentConverter = null;
    private int threadId;

    public FetchMessagesByConversation(Context context, int threadId) {
        this.context = context;
        this.delegate = (FetchMessagesByConversationInterface) context;
        this.threadId = threadId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected SmsConversation doInBackground(Void... params) {

//        LanguageDetector languageDetector = new LanguageDetector();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_ENABLE_SMS_ACCENT_CONVERT, false)) {
            if (accentConverter == null) {
                accentConverter = new AccentConverter(context);
            }
        }

        Uri uriSms = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            uriSms = Telephony.Sms.CONTENT_URI;
        } else {
            uriSms = Uri.parse("content://sms");
        }

        String[] projection = new String[]{
                Telephony.TextBasedSmsColumns.ADDRESS,
                Telephony.TextBasedSmsColumns.THREAD_ID,
                Telephony.TextBasedSmsColumns.PERSON,
                Telephony.TextBasedSmsColumns.BODY,
                Telephony.TextBasedSmsColumns.DATE,
                Telephony.TextBasedSmsColumns.TYPE,
                Telephony.TextBasedSmsColumns.READ,
                Telephony.TextBasedSmsColumns.SEEN};
        Cursor cursor = context.getContentResolver()
                .query(uriSms, projection, Telephony.TextBasedSmsColumns.THREAD_ID + " = " + threadId, null, null);
        if (cursor != null) {

            if (cursor != null) {

                if (cursor.moveToFirst()) {
                    do {
                        SmsMessage message = new SmsMessage();

                        message.setConversationId(cursor.getInt(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.THREAD_ID)));
                        message.setMessageNumber(cursor.getString(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.ADDRESS)));
                        message.setPerson(cursor.getString(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.PERSON)));
                        message.setType(cursor.getInt(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.TYPE)));
                        message.setMessageDate(cursor.getString(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.DATE)));
                        message.setContactId(getContactIDFromNumber(message.getMessageNumber(), context));
                        message.setSeen(cursor.getInt(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.SEEN)));
                        message.setRead(cursor.getInt(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.READ)));

                        String sourceMessage = cursor.getString(cursor.getColumnIndex(Telephony.TextBasedSmsColumns.BODY));
                        message.setMessageContent(sourceMessage);
                        //TODO a nyelvesítés még nem működik valószínűleg olyan java erőforrást használna amit androidon nem lehet
//                        Log.i("Detected language",languageDetector.detectLanguage(sourceMessage));
//                        if (!isMessageAlreadyExists(message)) {
                        if (sharedPreferences.getBoolean(SharedPreferencesUtils.KEY_ENABLE_SMS_ACCENT_CONVERT, false)) {
                            String accentedMessage = accentConverter.convertText(sourceMessage);
                            message.setMessageContent(accentedMessage);
                        } else {
                            message.setMessageContent(sourceMessage);
                        }

                        if ( smsConversation == null ) {
                            smsConversation = new SmsConversation();
                            smsConversation.setMessageList(new ArrayList<SmsMessage>());
                            smsConversation.setContactId(message.getContactId());
                            smsConversation.setPartnerPhoneNumber(message.getMessageNumber());
                        }

                        smsConversation.getMessageList().add(message);
                        Collections.sort(smsConversation.getMessageList());
//                        }
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            }
        }
        if (this.accentConverter != null) {
            this.accentConverter.releaseResources();
            this.accentConverter = null;
        }
        return smsConversation;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(SmsConversation smsConversation) {
        super.onPostExecute(smsConversation);
        delegate.AsyncTaskFinish(smsConversation);
    }

    public int getContactIDFromNumber(String contactNumber, Context context) {
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            String id = contactLookupCursor.getString(
                    contactLookupCursor.getColumnIndex(ContactsContract.Contacts._ID));
            String phoneNo = null;
            if (Integer.parseInt(contactLookupCursor.getString(contactLookupCursor.getColumnIndex(
                    ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                Cursor pCur = context.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                        new String[]{id}, null);
                while (pCur.moveToNext()) {
                    phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
                pCur.close();
                if (phoneNo.equals(contactNumber)) {
                    phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
            }
        }
        contactLookupCursor.close();

        return phoneContactID;
    }

}
