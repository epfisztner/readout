package uzoltan.readout.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.orm.query.Select;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;

import lombok.Setter;
import uzoltan.readout.R;
import uzoltan.readout.email.EmailMessage;
import uzoltan.readout.misc.SharedPreferencesUtils;

public class FetchEmails extends AsyncTask<String, Integer, ArrayList<EmailMessage>> {

    private ArrayList<EmailMessage> emails = new ArrayList<>();
    @Setter
    private FetchEmailsInterface delegate = null;
    private ProgressDialog progress;
    private Context context;
    private String username, password, imapHostName, dateString;
    private int startIndex, offset;
    private boolean isThereMore = true, swipeToRefresh;
    final String NEW_FORMAT = "MMM dd.";
    final String NEW_FORMAT_NEW = "HH:mm";
    private boolean textIsHtml = false;

    //constructor
    public FetchEmails(Context context, String username, String password, String imapHostName, int startIndex, int offset) {
        this.context = context;
        this.username = username;
        this.password = password;
        this.imapHostName = imapHostName;
        this.startIndex = startIndex;
        this.offset = offset;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<EmailMessage> doInBackground(String... args) {
        String emailFolder = args[0];
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            int savedEmailsNumber = sharedPreferences.getInt(SharedPreferencesUtils.KEY_SAVED_EMAILS_NUMBER, 40);
            Select savedEmailsQuery = Select.from(EmailMessage.class).where("message_folder = ?", new String[]{emailFolder}).limit(String.valueOf(savedEmailsNumber));
            if (savedEmailsQuery.count() == 0 || swipeToRefresh) {
                loadEmailMessages(emailFolder);
            } else {
                List<EmailMessage> savedEmails = savedEmailsQuery.list();
                emails.addAll(savedEmails);
            }
            sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_LOGIN_FAILED, false).apply();
            sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_LOGGED_IN, true).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emails;
    }

    @Override
    protected void onPostExecute(ArrayList<EmailMessage> emails) {
        super.onPostExecute(emails);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//        sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_IS_THERE_MORE_EMAIL, isThereMore).apply();
        delegate.AsyncTaskFinish(emails);
    }


    private void loadEmailMessages(String folder) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putBoolean(SharedPreferencesUtils.KEY_SWIPE_REFRESH, false).apply();
        Properties properties = new Properties();
        properties.put("mail.store.protocol", "imaps");
        properties.put("mail.imap.ssl.trust", "*");

        Session emailSession = Session.getDefaultInstance(properties);
        Store store = null;
        try {
            store = emailSession.getStore("imaps");

            store.connect(imapHostName, username, password);

            if (store.isConnected()) {
                // list all folder names
                Folder[] folders = store.getDefaultFolder().list("*");
                for (Folder f : folders) {
                    String folderName = "";
                    folderName = f.getFullName();
                    Folder emailFolder = store.getFolder(folderName);

                    if (emailFolder.getName().equals(folder)) {
                        emailFolder.open(Folder.READ_WRITE);

                        //javamail Message array object
                        Message[] messages = emailFolder.getMessages();
                        int j = messages.length - 1;

                        //we save an offset amount of emailFolders' data
                        for (int i = j - startIndex; i > j - startIndex - offset && i > (-1); i--) {

                            //asynctask has a cancel method, this method reads that
                            //if the caller activity offscreen, we stop the email loading
                            if (isCancelled()) {
                                break;
                            }

                            if (i == 0) {
                                isThereMore = false;
                            }

                            //our storing class
                            EmailMessage emailMessage = new EmailMessage();

                            emailMessage.setMessageFolder(emailFolder.getName());
                            //address object needs decoding
                            Address[] senderAddress = messages[i].getFrom();
                            Address[] recipientsAddress = messages[i].getAllRecipients();
                            String tempAddress = "";
                            if (!senderAddress[0].toString().contains(username)) {
                                tempAddress = senderAddress[0].toString();
                            } else {
                                tempAddress = recipientsAddress[0].toString();
                            }
                            tempAddress = MimeUtility.decodeText(tempAddress);

                            //even after decoding, some addresses needs more work
                            if (tempAddress.contains("=?")) {
                                String[] AddressParts = tempAddress.split("\\?=");
                                emailMessage.setMessageAddress(AddressParts[1].substring(2));
                            } else {
                                emailMessage.setMessageAddress(tempAddress);
                            }

                            emailMessage.setRead(messages[i].isSet(Flags.Flag.SEEN));

                            Date date = messages[i].getSentDate();
                            boolean fromToday = DateUtils.isToday(date.getTime());
                            SimpleDateFormat sdf;

                            //if the email is from this day, then we store the date in HH:mm format
                            if (fromToday) {
                                sdf = new SimpleDateFormat(NEW_FORMAT_NEW, Locale.US);
                            } else {
                                sdf = new SimpleDateFormat(NEW_FORMAT, Locale.US);
                            }

                            dateString = sdf.format(date);

                            emailMessage.setMessageDate(dateString);

                            emailMessage.setMessageSubject(messages[i].getSubject());

                            //handling the message content
                            Object msgContent = messages[i].getContent();
                            String content = "";
                            if (msgContent instanceof Multipart) {

                                Multipart multipart = (Multipart) msgContent;

                                Log.e("BodyPart", "MultiPartCount: " + multipart.getCount());

                                for (int k = 0; k < multipart.getCount(); k++) {

                                    BodyPart bodyPart = multipart.getBodyPart(k);

                                    String textContent = getText(bodyPart);

                                    if (textContent != null && textIsHtml)
                                        content = textContent;

                                    String disposition = bodyPart.getDisposition();

                                    if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) {
                                        DataHandler handler = bodyPart.getDataHandler();
                                        content = content + "\n<br>" + context.getString(R.string.email_attachment_text) + "\n<br>" + handler.getName();
                                    } else if (content.isEmpty()) {
                                        content = bodyPart.getContent().toString();
                                    }
                                }
                            } else
                                content = messages[i].getContent().toString();
                            emailMessage.setMessageContent(content);

                            emailMessage.save();

                            emails.add(emailMessage);
                        }
                        emailFolder.close(false);
                    }
                }
                store.close();
            }
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected String getText(Part p) throws MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            String s = (String) p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }
        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp);
                    textIsHtml = false;
                    //continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    textIsHtml = true;
                    if (s != null)
                        return s;
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }
        return null;
    }

}