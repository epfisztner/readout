package uzoltan.readout.asynctask;

import java.util.ArrayList;

import uzoltan.readout.email.EmailMessage;

public interface FetchEmailsInterface {

    void AsyncTaskFinish(ArrayList<EmailMessage> EmailInbox);
}
